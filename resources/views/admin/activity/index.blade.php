@extends('admin.app')

@section('title', 'SKR | User')
@section('content')
<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <form action="" id="form-search">
                            {{__('layout.from')}}: <input readonly id="from-date" name="from" value="">
                            - {{__('layout.to')}}: <input readonly id="to-date" name="to" value="">
                            <a href="" id="reset"><i class="fa fa-refresh" aria-hidden="true"></i></a>
                            <div class="float-right">
                                <button id="clear" class="btn btn-danger">
                                    <i class="fa fa-trash" aria-hidden="true"></i> {{ trans('action.clear') }}
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="card-body ">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>{{__('layout.user')}}</th>
                                        <th>{{__('layout.ip_address')}}</th>
                                        <th>{{__('layout.history')}}</th>
                                        <th>{{__('layout.log_time')}}</th>
                                        <th style="width:200px">{{__('layout.more_info')}}</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
</div>
@endsection
@push('page-styles')
    <!--Jquery ui -->
    <link rel="stylesheet" href="{{ asset('lib/jquery/jquery-ui/jquery-ui.min.css') }}">
    <style>
        #from-date, #to-date {
            border-radius: 5px;
            padding: 2px 6px;
            margin-right: 4px;
            border: 1px solid #9aa1e4;
            color: #421818;
        }
    </style>
@endpush
@push('page-scripts')
<!--Jquery ui -->
<script src="{{ asset('lib/jquery/jquery-ui/jquery-ui.min.js') }}"></script>
<!--Sweetalert-->
<script src="{{ asset('lib/alert/sweetalert.min.js') }}"></script>
<script>
    let data_search = [];
    load_data();
    $('#from-date').datepicker({
        dateFormat: 'yy-mm-dd',
        beforeShow: function() {
            setTimeout(function(){
                $('.ui-datepicker').css('z-index', 9999);
            }, 0);
        },
        onSelect: function(dateText) {
            data_search['from'] = this.value;
            $('#dataTable').DataTable().destroy();
            load_data(data_search);
        }
    });

    $('#to-date').datepicker({
        dateFormat: 'yy-mm-dd',
        beforeShow: function() {
            setTimeout(function(){
                $('.ui-datepicker').css('z-index', 9999);
            }, 0);
        },
        onSelect: function(dateText) {
            data_search['to'] = this.value;
            $('#dataTable').DataTable().destroy();
            load_data(data_search);
        },
        // minDate: null
    });

    $("#reset").on('click', function (e) {
        e.preventDefault();
        data_search = [];
        $("#from-date, #to-date").val('');
        $('#dataTable').DataTable().destroy();
        load_data();
    });

    $("#clear").on('click', function(e){
        e.preventDefault();
        swal("{{ trans('layout.clear_logs') }}").then((value) => {
            if(value){
                $.ajax({
                    url: "{{route('activity.clear')}}",
                    type: 'post',
                    data: {
                        'from': data_search['from'],
                        'to' : data_search['to']
                    },
                    success: function (data) {
                        location.reload(true);
                    },
                    error: function (e) {
                        console.log(e.message);
                    }
                });
            }
        });
    });

function load_data(data_search=[]){
    $("#dataTable").DataTable({
        processing: true,
        pageLength: 10,
        serverSide: true,
        ordering: false,
        searching: false,
        lengthChange: false,
        ajax: {
            url: "{{ route('activity.index') }}",
            type: 'GET',
            data: data_search
        },
        columns: [
            { "data": "name"},
            {
                "data": "properties",
                "render": function(data) {
                    let ip =JSON.parse(data.replace(/&quot;/g,'"')).ip;
                    return ip;
                }
            },
            { "data": "description" },
            { "data": "created_at" },
        ],
        columnDefs: [{
            "targets": 4,
            "data": "properties",
            "render": function(data) {
                let browser =JSON.parse(data.replace(/&quot;/g,'"')).browser;
                return browser;
            }
        }],
    });
}
</script>
@endpush

