<?php
return [
    'galleries' => 'ギャラリー',
    'file_manager' => 'ファイル管理',
    'gallery_list' => 'ギャラリー一覧',
    'add_gallery' => '新しいギャラリーを追加',
    'edit_gallery' => 'ギャラリーを編集',
    'title' => '題名',
    'description' => '説明',
    'image' => '画像',
    'link' => 'リンク',
    'type' => 'タイプ',
    'order' => 'ソート順',
    'status' => '状態',
    'active' => '活動',
    'choose_image' => '画像を選択',
    'upload_files' => 'ファイルをダウンロードする',

    //Gallery
    'galleries' => 'ギャラリー',
    'gallery_list' => 'ギャラリーリスト',
    'add_gallery' => '新しいギャラリーを追加する',
    'edit_gallery' => 'ギャラリーを編集',

    //Banner
    'banners' => 'バナー',
    'banner_list' => 'バナーリスト',
    'add_banner'=> '新しいバナーを追加する',
    'edit_banner'=> 'バナーを編集する'
];
