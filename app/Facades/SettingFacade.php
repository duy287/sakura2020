<?php

namespace App\Facades;

use App\Supports\SettingStore;
use Illuminate\Support\Facades\Facade;

class SettingFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     * @author Minh Hao
     */
    protected static function getFacadeAccessor()
    {
        return SettingStore::class;
    }
}
