@extends('admin.app')

@section('title', 'SKR | Users')

@section('content')
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-9">
                    <div class="card card-primary card-outline">
                        <div class="card-header"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{__('layout.edit_group')}}</div>
                        <div class="card-body">
                            <form action="{{route('groups.update', $group->id)}}" method="POST">
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <label for="name">{{__('layout.name')}}</label>
                                    <input type="text" id="name" name="name" value="{{ old('name', $group->name) }}" 
                                        class="form-control @error('name') is-invalid @enderror">
                                    @error('name')<div class="invalid-feedback">{{ $message }}</div>@enderror
                                </div>
                                <div class="form-group">
                                    <label for="description">{{__('layout.description')}}</label>
                                    <textarea name="description" rows="3" placeholder="Short description" 
                                    class="form-control">{{ old('description', $group->description) }}</textarea>
                                </div>
                                <div class="widget meta-boxes">
                                    <div class="widget-title">
                                        <h4>{{__('action.select_user')}}</h4>
                                    </div>
                                    <div class="widget-body">
                                        <label>{{__('action.select_user')}}:</label>
                                        <select id ="select_user" class="select-user-box" data-placeholder="{{__('role.select_user')}}" style="width: 50%;">
                                            @foreach($users as $key=>$user)
                                                <option value="{{ $user->id }}">
                                                    {{ $user->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <input name="user_ids" value="" hidden/>
                                        <a href="" id="adduser">ADD</a>
                                        <ul class="list_user">
                                            @foreach($users as $user)
                                                @if(in_array($user->id, $user_selected_ids))
                                                <li id="item_{{$user->id}}">
                                                    <a onClick="removeItem({{ $user->id }})">x</a>
                                                    {{$user->name}}
                                                </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i> {{__('action.save')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('page-scripts')
<!-- Select 2 -->
<script src="{{ asset('lib/select2/js/select2.full.min.js') }}"></script>
<script>
    //Initialize Select2 Elements
    $('.select-user-box').select2();

    //get list user_ids in group
    var arr_ids = [
            @foreach($user_selected_ids as $val)
                '{{ $val }}',
            @endforeach
        ];
    //user_ids element
    var user_ids_element = $("input[name='user_ids']");
    //init data for user_ids_element
    user_ids_element.val(arr_ids);

    $("#adduser").click(function(e){
        e.preventDefault();
        let name = "";
        let id = $("#select_user").val();
        if(arr_ids.indexOf(id) < 0){
            arr_ids.push(id);
            user_ids_element.val(arr_ids);
            name = $("#select_user option:selected" ).text();
            $('.list_user').append(`<li id="item_${id}"><a onClick="removeItem(${id})">x</a>${name}</li>`);
        }
        console.log(arr_ids);
    });

    function removeItem(id){
        let pos = arr_ids.indexOf(id + "");
        if(pos > -1){
            $("#item_" + id).remove();
            arr_ids.splice(pos, 1);
            user_ids_element.val(arr_ids);
        }
        console.log(arr_ids);
    }

</script>
@endpush
