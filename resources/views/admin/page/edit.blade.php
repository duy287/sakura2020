@extends('admin.app')

@section('title', 'SKR | Users')

@section('content')
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-8">
                    <div class="card card-primary card-outline">
                        <div class="card-header"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{__('layout.edit_page')}}</div>
                        <div class="card-body">
                            <form action="{{route('pages.update', $page->id)}}" method="POST">
                                @csrf
                                @method('PUT')
                                <div class="form-group">  
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="vi-tab" data-toggle="tab" href="#vi" role="tab" aria-controls="vi" aria-selected="false">{{__('layout.vietnamese')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="ja-tab" data-toggle="tab" href="#ja" role="tab" aria-controls="ja" aria-selected="true">{{__('layout.japanese')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en" aria-selected="false">{{__('layout.english')}}</a>
                                        </li>
                                    </ul>
                                
                                    <div class="tab-content tabs-trans" id="myTabContent">
                                        <div class="tab-pane fade show active" id="vi" role="tabpanel" aria-labelledby="vi-tab">
                                            <div class="form-group">
                                                <label for="name">{{__('layout.name')}}</label>
                                                <input type="text" name="vi[title]" value="{{ old('vi.title', $page->{'title:vi'}) }}" class="form-control
                                                @error('ja.title') is-invalid @enderror">
                                                @error('ja.title')<div class="invalid-feedback">{{ $message }}</div>@enderror
                                            </div>
                                            <div class="form-group">
                                                <label>{{__('layout.content')}}</label>
                                                <textarea class="form-control" rows="3" name="vi[content]">{{ old('vi.content', $page->{'content:vi'}) }}</textarea>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="ja" role="tabpanel" aria-labelledby="ja-tab">
                                            <div class="form-group">
                                                <label for="name">{{__('layout.title')}}</label>
                                                <input type="text" name="ja[title]" value="{{ old('ja.title', $page->{'title:ja'}) }}" 
                                                    class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>{{__('layout.content')}}</label>
                                                <textarea class="form-control" rows="3" name="ja[content]">{{ old('ja.content', $page->{'content:ja'}) }}</textarea>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
                                            <div class="form-group">
                                                <label for="name">{{__('layout.title')}}</label>
                                                <input type="text" name="en[title]" value="{{ old('en.title', $page->{'title:en'}) }}" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>{{__('layout.content')}}</label>
                                                <textarea class="form-control" rows="3" name="en[content]">{{ old('en.content', $page->{'content:en'}) }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="type">Type</label>
                                    <select id="type" class="form-control" name="type">
                                        <option value="1">About us</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="avatar">{{__('layout.image')}}</label>
                                    <div>
                                        {{-- <button id="ckfinder-modal" class="btn btn-primary btn-sm">{{__('action.browse_server')}}</button>
                                        <input id="ckfinder-input" name="image" hidden> --}}
        
                                        <label class="upload-photo btn btn-success btn-sm" for="upload-photo">{{__('action.upload_from_client')}}</label>
                                        <input type="file" onchange="readImage(this)" name="image" id="upload-photo" hidden/>    
                                    </div>
                                    <img id="img-preview" src="{{ asset("storage/images/pages/$page->image") }}" alt="" class="img-thumbnail">
                                    @error('image')<div class="text-danger">{{ $message }}</div>@enderror
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i> {{__('action.save')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('page-scripts')
<script>
    var button = $('#ckfinder-modal');
    button.click(function(e){
        e.preventDefault();
        selectFileWithCKFinder('#ckfinder-input');
    });
    </script>
@endpush