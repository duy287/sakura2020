@extends('admin.app')

@section('title', 'SKR | Dashboard')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header" style="border-bottom: #d2d6de 1px solid">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Dashboard</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item ">{{__('layout.dashboard')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4 col-xs-12">
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3>{{ $data['count_posts'] }}</h3>
                                <p>{{__('Post::post.posts')}}</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                            </div>
                            <a href="{{url('/admin/posts')}}"class="small-box-footer">{{__('layout.more_info')}} <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-xs-12">
                        <div class="small-box bg-green">
                            <div class="inner">
                                <h3>{{$data['count_users']}}</h3>
                                <p>{{__('layout.users')}}</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                            </div>
                            <a href="{{url('/admin/users')}}"class="small-box-footer">{{__('layout.more_info')}} <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-xs-12">
                        <div class="small-box bg-yellow">
                            <div class="inner">
                                <h3>{{$data['count_contacts']}}</h3>
                                <p>{{__('layout.contacts')}}</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            </div>
                            <a href="{{url('/admin/contacts')}}"class="small-box-footer">{{__('layout.more_info')}} <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                <div>
                    <!-- BAR CHART -->
                    <div class="row">
                        <div class="col-md-8 col-sm-12">
                            <div class="box box-danger">
                                <div class="box-header with-border">
                                    <h3 class="box-title">News Chart</h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>
                                <div class="box-body" style="">
                                    {{-- <canvas id="myChart" width="200" height="140"></canvas> --}}
                                    <div id="chart_div" width="200" height="140"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="calendar-wrapper">
                                <div id="calendari"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('page-scripts')
<script type="text/javascript" src="{{ asset('lib/calendar/calendar.js') }}"></script>

<script>
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
    var data = google.visualization.arrayToDataTable([
        ['Year', 'Sales'],
        ['2013',  1000],
        ['2014',  1170],
        ['2015',  660],
        ['2016',  1030]
    ]);

    var options = {
        title: 'Company Performance',
        hAxis: {title: 'Year',  titleTextStyle: {color: '#333'}},
        vAxis: {minValue: 0}
    };

    var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
    chart.draw(data, options);
    }


</script>
@endpush

@push('page-styles')
<style>
    .calendar-wrapper {
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
        min-height: 100%;
        padding: 0px;
        background: #fff;
    }
    #calendari {
        margin: 5px 0;
        position: relative;
        overflow: hidden;
        min-height: 345px;
        min-width: 350px;
        font-size: 14px;
        box-shadow: 0px 1px 4px rgba(0,0,0,0.4);
    }
    #calendari table {
        border-collapse: collapse;
        table-layout: fixed;
        width: 350px;
        box-shadow: 0px 1px 3px rgba(0,0,0,0.2);
        background-color: #fff;
        position: absolute;
        top: 0;
        left: 0;
        transform: translateX(0);
        transition: all 0.3s ease;
    }
    #calendari table.actiu {
        transform: translateX(0px)top;
    }
    #calendari table.inactiu {
        transition: all 0.3s 0.01s ease;
    }
    #calendari table.amagat-esquerra {
        transform: translateX(-299px);
    }
    #calendari table.amagat-dreta {
        transform: translateX(300px);
    }
    #calendari td,th {
        text-align: center;
        background-color: #fff;
    }
    #calendari th {
        padding: 10px;
    }
    #calendari tr:first-child th {
        font-size: 20px;
        font-weight: bold;
        border-left: none;
        border-top: none;
    }
    #calendari td:last-child, th:last-child {
    border-right: none;
    }

    #calendari th {
        border-top: 1px solid rgba(0,0,0,0.1);
        border-right: 1px solid rgba(0,0,0,0.1);
        background-color: #9b59b6;
        color: #fff;
        text-shadow: 0px -1px 0px rgba(0,0,0,0.2);
        font-weight: normal;
    }
    #calendari th .any {
        font-size: 12px;
        font-weight: normal;
        display: block;
        text-shadow: none;
        color: rgba(0,0,0,0.4);
    }
    #calendari tr:nth-child(2) th {
        padding: 5px;
    }
    #calendari td {
        padding: 0;
        border-bottom: 1px solid rgba(0,0,0,0.05);
    }
    #calendari td>span {
        color: #555;
        padding: 10px;
        display: block;
        border: 2px solid transparent;
        transition: border 0.3s ease;
    }

    #calendari td:nth-child(even)>span {
        background-color: rgba(0,0,0,0.02);
    }
    #calendari td:last-child>span,
    td:nth-child(6)>span {
        color: #9b59b6;
    }
    #calendari td.avui>span {
        font-weight: bold;
        background-color: #9b59b6;
        color: #fff;
        border: 2px solid rgba(0,0,0,0.1);
    }
    #calendari td.fora > span {
        opacity: 0.2;
    }
    #calendari td > span:hover {
        background: #a57cb6;
        color: #fff;
    }
    .boto-next, .boto-prev {
        background: rgba(0,0,0,0.1);
        color: #fff;
        font-family: inherit;
        border: none;
        font-size: 18px;
        font-weight: bold;
        text-shadow: inherit;
        padding: 2px 10px 5px 10px;
        line-height: 1px;
        height: 30px;
        width: 30px;
        vertical-align: middle;
        border-radius: 100%;
        position: absolute;
        top: 15px;
    }
    .boto-next { right: 10px; padding-left: 13px; }
    .boto-prev { left: 10px; padding-right: 13px;}
    .boto-next:hover,
    .boto-prev:hover {
        background: rgba(0,0,0,0.2);
    }
    #calendari button:hover { cursor: pointer; }
    #calendari button:focus { outline: none; }

    #calendari footer {
    text-align: center;
    color: #ddd;
    font-weight: normal;
    text-shadow: 0px -1px 0px rgba(0, 0, 0, 0.2);
    font-size: 0.8em;
    padding: 20px;
    }

    #calendari footer a,
    footer a:link {
        color: #fff;
        text-decoration: none;
    }
</style>
@endpush