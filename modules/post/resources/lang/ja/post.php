<?php
return [
    'posts' => 'ポスト',
    'post_list' => '投稿リスト',
    'add_post' => '新しい投稿を追加',
    'edit_post' => '投稿を編集',
    'slug'  => 'リンク',
    'author'=> '著者',
    'type' => 'タイプ',
    'title'=> '題名',
    'status' => '状態',
    'tags' => 'タグ',
    'featured_image' => '注目の画像',
    'category' => 'カテゴリー',
    'belong_to_category' => 'カテゴリーに属する',
    'active' => 'アクティブ',
    'content' => 'コンテンツ'
];
