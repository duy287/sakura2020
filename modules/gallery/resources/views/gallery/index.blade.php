@extends('admin.app')

@section('title', 'SKR | Users')

@section('content')
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <span class="lead">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{__('Gallery::gallery.galleries')}}</div>
                            </span>
                        <div class="card-body">
                            <div class="form-group">  
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="gallery-tab" data-toggle="tab" href="#gallery" role="tab" aria-controls="gallery" aria-selected="true">{{__('Gallery::gallery.galleries')}}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="upload-tab" data-toggle="tab" href="#upload" role="tab" aria-controls="upload" aria-selected="false">{{__('Gallery::gallery.upload_files')}}</a>
                                    </li>
                                </ul>
                            
                                <div class="tab-content tabs-trans" id="myTabContent">
                                    <!--Gallery tab content -->
                                    <div class="tab-pane fade show active" id="gallery" role="tabpanel" aria-labelledby="gallery">
                                        <div class="row gallery">
                                            @foreach($galleries as $gallery)
                                            <div class="col-lg-3 col-md-4 col-xs-6 gallery-item p-1">
                                                <div style="position: relative; width:100%">
                                                    <a href="{{asset("storage/images/galleries/".$gallery->image)}}" class="show-img">
                                                        <img class="img-fluid img-thumbnail fixed-size" src="{{asset("storage/images/galleries/".$gallery->image)}}">
                                                    </a>
                                                    <div class="overlay">
                                                        <span class="btn btn-danger btn-sm" id="{{$gallery->id}}"
                                                            onclick = "checkItemExisted($(this).attr('id'))" data-toggle="modal" data-target="#confirmDeleteItem">
                                                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                                                        </span>
                                                        <a class="btn btn-success btn-sm" target="_blank" href="{{asset("storage/images/galleries/".$gallery->image)}}">
                                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                                        </a>
                                                        <span class="pull-right">{{\Str::limit($gallery->image, 22)}}<br>
                                                            Size: {{ number_format($gallery->size / 1024, 2)}} KB
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <!--Upload tab content-->
                                    <div class="tab-pane fade" id="upload" role="tabpanel" aria-labelledby="upload">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <form method="POST" action="" enctype="multipart/form-data">
                                                    @csrf 
                                                    <label for="">{{__('Gallery::gallery.choose_image')}}</label>
                                                    <div class="form-group">
                                                        <label class="upload-photo btn btn-success btn-sm" for="upload-photo">{{__('action.choose_file')}}</label>
                                                        <input type="file" onchange="readImage(this)" name="image" id="upload-photo" hidden/>
                        
                                                        <img id="img-preview" src="" alt="" class="img-thumbnail">
                                                        @error('image')<div class="text-danger">{{ $message }}</div>@enderror
                                                    </div>
                                                    <hr>
                                                    <div class="text-left">
                                                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="modal fade" id="confirmDeleteItem" tabindex="-1" role="dialog" aria-labelledby="modelLabel"
          aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    {{__('action.delete_confirm')}}
                </div>
                <div id="confirmMessage" class="modal-body">
                    {{__('action.delete_sure', ['obj'=>'Image'])}}
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnConfirmDelete" class="btn btn-danger btn-ok"
                            onclick="deleteItem($(this).val())">
                        {{__('action.delete')}}
                    </button>
                    <button type="button" id="confirmCancel" class="btn btn-secondary" data-dismiss="modal"
                            data-toggle="modal" data-target="#confirmDeleteUser">
                        {{__('action.cancel')}}
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('page-styles')
    <link rel="stylesheet" href="{{ asset('lib/magnific-popup/magnific-popup.min.css') }}">
@endpush

@push('page-scripts')
<script src="{{ asset('lib/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $(".gallery").magnificPopup({
            delegate: "a.show-img",
            type: "image",
            tLoading: "Loading image #%curr%...",
            mainClass: "mfp-img-mobile",
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
            }
        });
    });

    function checkItemExisted(id) {
        console.log(id);
        $('#btnConfirmDelete').val(id);
    }

    function deleteItem(id) {
        let delete_URL = "{{ url('admin/galleries') }}/" + id;
        let request = $.ajax({
            url: delete_URL,
            type: "DELETE"
        }).done(function (data) {
            $("#confirmDeleteItem").modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            
            if(data.status_code == 500){
                iziToast.success({
                    title: 'Error',
                    message: data.message,
                    position: 'topRight'
                });
            }
            else {
                $(`#${data.row_id}`).closest("div.gallery-item").remove();
                iziToast.success({
                    title: 'Message',
                    message: data.message,
                    position: 'topRight'
                });
            }
        });
    }
</script>
@endpush
