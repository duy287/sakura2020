<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SAKURA MAIL</title>
</head>
<style>
    .wrapper-mail {
        max-width: 500px;
        position: relative;
        background-color: #fffdff;
        border-radius: 10px;

    }

    .link {
        background: #19cca3;
        color: #ffffff;
        padding: 10px 20px;
        font-family: Helvetica, sans-serif;
        font-size: 18px;
        font-weight: 600;
        line-height: 120%;
        text-decoration: none;
        text-transform: none;
        border-radius: 10px;
        border: none;
        margin-bottom: 10px;
        width: 126px;
    }

    @media only screen and (max-width: 500px) {
        .wrapper-mail {
            width: 98%;
            border-radius: 10px;
        }
        .container590 {
            margin-bottom: 20px;
        }

    }
</style>

<body style="  padding-top: 20px ">
<table width="100%" cellpadding="0" cellspacing="0" >
    <tr>
        <td align="center">
            <table align="center" width="100" cellpadding="0" cellspacing="0" class="container590">
                <tr>
                    <td align="center">

                        <table align="center" width="100" cellpadding="0" cellspacing="0"
                               class="container590">

                            <tr>
                                <td align="center" height="70" style="height:70px;" >
                                    <a href=""
                                       style="display: block; border-style: none !important; border: 0 !important;">
                                        <img
                                            width="100" border="0" style="display: block; width: 200px;"
                                            src="https://yaviet.com/wp-content/uploads/2018/07/LOGO-YAVIET-web.png"
                                            alt="logo"/>
                                        {{--                                                src="https://reviewsnice.com/sakura/img/page/logo.png" alt="logo"/>--}}
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>
        </td>
    </tr>
</table>

@yield('mail-content')

</body>
</html>
