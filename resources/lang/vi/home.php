<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menu Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'home' => 'trang chủ',
    'about' => 'Thông tin',
    'service' => 'DỊCH VỤ',
    'customer' => 'KHÁCH HÀNG',
    'contact' => 'LIÊN HỆ',
    'vi'=>'TIẾNG VIỆT',
    'jp'=>'TIẾNG NHẬT',
    'en'=>'TIẾNG ANH',

    'banner-t' => 'Ensuring the quality of service Sakura company wishes to accompany customers',
    'skr' => 'Sakura Ecology Company',
    'banner-b' => 'hopes to make customers satisfied with some our servies.',

    'about-us' => 'ABOUT US',
    'about-content1' => 'Established in Ho Chi Minh City in December, 2007 with Our Parent Company is Kofu
                                    building service Ltd. that put the base on Yamanashi Prefecture Kofu City in
                                    Japan .Kofu building service Ltd. has over 50 years experience in the field of
                                    integrated building management with over 200 customers around Kofu city such as
                                    Tokyo, Yokohama, Nagano, Shizuoka ….',
    'about-content2' => ' With the motto is whole - hearted -
                                        enthusiasm, we will give you a safe space and comfort.Progress to Vietnam with
                                        the aim of beginning and gradual development of building maintenance in this
                                        growing market , and will provide services with high quality and This Vietnam
                                        advancement was large and the [birumen] industry of Japan was made to be started
                                        in a developing Vietnamese market, and a Vietnamese enterprise and the
                                        advancement Japan enterprise wanted to help you and landed providing with high
                                        service of a super-modern quality. We wish to take advantage of the know-how and
                                        experience accumulated in Japan to contribute to the development of the building
                                        maintenance sector in Vietnam.',
    'about-thanks'=>'Thank you so kindly for your support and encouragement',
    'read-more'=>'Read more',
    'compact'=>'Compact',

    'business'=>'Business Outline',
    'guiding'=>'Guiding Principles',
    'execution'=>'Execution Order',
    'all-sv'=>'ALL SERVICES',
    'operation'=>'Operation Content',
    'office'=>'Office Building',
    'office-t1'=>'Facilities management',
    'office-t'=>'Cleaning Business',
    'office-t2'=>'Cleaning Business',
    'office-t3'=>'Environment Relation',
    'office-t4'=>'Construction Business',
    'office-c1'=>'Equiment operation management bya penmanent residence person, interction check of equipment, water tank 
                    clearing and check, fire-fighting equipment check, cleak fire-fighting equipment check. Clear of electric euipment.',
    'office-c2'=>'Daily life\'s cleaning: lobby, pestroom, car park, such as stairs, conference such as parking lots.',
    'office-c3'=>'Infection check of equipment, water tank cleaning and check, fire-fighting equipment check, control of maintenance of a septic tank..',
    'office-c4'=>'Budding mending business, budding work, fire fighting equipment mending.',

    'supermarket-d'=>'Supermarket & Department Store ',
    'supermarket'=>'Supermarket &',
    'department'=>'Department Store',
    'supermarket-t1'=>'Cleaning Business:',
    'supermarket-t2'=>'Management of affairs:',
    'supermarket-t3'=>'Construction Business:',
    'supermarket-t4'=>'Facilities management:',
    'supermarket-c1'=>'Infection check of equipment, water tank cleaning and check, fire-fighting equipment check, control of maintenance of a septic tank.',
    'supermarket-c2'=>'Infection check of equipment, water tank cleaning and check, fire-fighting equipment check, control of maintenance of a septic tank.',
    'supermarket-c3'=>'Infection check of equipment, water tank cleaning and check, fire-fighting equipment check, control of maintenance of a septic tank.',
    'supermarket-c4'=>'Infection check of equipment, water tank cleaning and check, fire-fighting equipment check, control of maintenance of a septic tank.',

    'apartment'=>'Apartment House',
    'apartment-t1'=>'Facilities management',
    'apartment-t'=>'Cleaning Business',
    'apartment-t2'=>'Cleaning Business',
    'apartment-t3'=>'Environment Relation',
    'apartment-t4'=>'Construction Business',
    'apartment-t5'=>'Construction Business',
    'apartment-c1'=>'Equiment operation management bya penmanent residence person, interction check of equipment, water tank 
    clearing and check, fire-fighting equipment check, cleak fire-fighting equipment check. Clear of electric euipment.',
    'apartment-c2'=>'Daily life\'s cleaning: lobby, pestroom, car park, such as stairs, conference such as parking lots.',
    'apartment-c3'=>'Infection check of equipment, water tank cleaning and check, fire-fighting equipment check, control of maintenance of a septic tank..',
    'apartment-c4'=>'Budding mending business, budding work, fire fighting equipment mending.',
    'apartment-c5'=>'Peceipts work, attendance business: equipment check, mending construction etc move.',

    'factory'=>'Factory',
    'factory-t1'=>'Cleaning Business:',
    'factory-t2'=>'Management of affairs:',
    'factory-t3'=>'Construction Business:',
    'factory-t4'=>'Facilities management:',
    'factory-c1'=>'Infection check of equipment, water tank cleaning and check, fire-fighting equipment check, control of maintenance of a septic tank.',
    'factory-c2'=>'Infection check of equipment, water tank cleaning and check, fire-fighting equipment check, control of maintenance of a septic tank.',
    'factory-c3'=>'Infection check of equipment, water tank cleaning and check, fire-fighting equipment check, control of maintenance of a septic tank.',
    'factory-c4'=>'Infection check of equipment, water tank cleaning and check, fire-fighting equipment check, control of maintenance of a septic tank.',

    'planting'=>'Planting Service',
    'planting-t1'=>'Facility Management',
    'planting-t2'=>'Cleaning business',
    'planting-t3'=>'Construction business',
    'planting-t4'=>'Management of affairs',
    'planting-c1'=>'Infection check of equipment, water tank cleaning and check, fire-fighting equipment check, control of maintenance of a septic tank.',
    'planting-c2'=>'Infection check of equipment, water tank cleaning and check, fire-fighting equipment check, control of maintenance of a septic tank.',
    'planting-c3'=>'Infection check of equipment, water tank cleaning and check, fire-fighting equipment check, control of maintenance of a septic tank.',
    'planting-c4'=>'Infection check of equipment, water tank cleaning and check, fire-fighting equipment check, control of maintenance of a septic tank.',

    'clean'=>'Clean Room',
    'clean-t1'=>'Facilities Management',
    'clean-t2'=>'Cleaning business',
    'clean-t3'=>'Construction business',
    'clean-t4'=>'Management of affairs',
    'clean-c1'=>'Infection check of equipment, water tank cleaning and check, fire-fighting equipment check, control of maintenance of a septic tank.',
    'clean-c2'=>'Infection check of equipment, water tank cleaning and check, fire-fighting equipment check, control of maintenance of a septic tank.',
    'clean-c3'=>'Infection check of equipment, water tank cleaning and check, fire-fighting equipment check, control of maintenance of a septic tank.',
    'clean-c4'=>'Infection check of equipment, water tank cleaning and check, fire-fighting equipment check, control of maintenance of a septic tank.',

    'h-service'=>'HIGHLIGHTED SERVICES',
    'h-service-title'=>'Maintenance services; cleaning and caring of building',
    'h-service-content'=>'Providing high quality services; improving skills based on training efforts; creating a safeand secure working environment; 
    taking care of c ustomer oroughly.In addition there is a total of house cleaning, daily cleaning, office building, factory supermarket, school hospital . 
    providing green, clean and beautiful space for customers.',

    'floor'=>'FLOOR CLEANING',
    'floor-t'=>'clean and beautiful space for customer',

    'glass'=>'GLASS CLEANING',
    'glass-t'=>'clean and beautiful space for customer',

    'bonsai'=>'BONSAI CARE',
    'bonsai-t'=>'clean and beautiful',

    'pets'=>'PETS CONTROL',
    'pets-t'=>'clean and beautiful space for customer',

    'feedback'=>'Feedback',
    'news'=>'News',
    'next'=>'Next',
    'vn-company'=>'Vietnam Company',
    'jp-company'=>'Japan Company',

    'contact-us'=>'CONTACT US',

    'business-page'=>'Business Outline',
    'business-page-t'=>'Five promises to custommer\'s. Be sure to keep five promises.',
    'business-1'=>'To give prominence to communicate with customers.',
    'business-2'=>'Operational after understanding the customer\'s facilities.',
    'business-3'=>'Proposed to meet the needs of potential customers.',
    'business-4'=>'Provide operation management for customer\'s clearly.',
    'business-5'=>'Provide comport space and fresh for customer\'s.',
    'Company-name'=>'Company name',
    'skr-name'=>'SAKURA ECOLOGY CO.,LTD',

    'guiding-r1'=>'Promote Japan of building maintenance services in Vietnam, becoming one of the companies providing the best services.',
    'guiding-r2'=>' Customer-focused “trust” and “grateful”: providing hight quality service, advanced skill based training of education
                     and practise, create the workplace safe and secure.',
    'why-choose'=>'WHY CHOOSE US',
    'guiding-management'=>'Management Policy',
    'guiding-create'=>'Create valuable space',
    'guiding-t1'=>'For the customer:',
    'guiding-t2'=>'For the environment:',
    'guiding-t3'=>'For the society:',
    'guiding-c1'=>'Providing hight quality service and fast according to customer’s expectations.',
    'guiding-c2'=>'Establishing environmental managenment systems to continously improve and promote environmental protection.',
    'guiding-c3'=>'Advanced knowledge,competence and personality of each individual to be the training of employees can contribute to society.',

    'execution-h'=>'From receipt of the request to follow after service',
    'execution-t1'=>'Listen requirements',
    'execution-t2'=>'Investigation',
    'execution-t3'=>'Demonstration',
    'execution-t4'=>'Proposition Estimate',
    'execution-t5'=>'Contract',
    'execution-t6'=>'Acceptance report',
    'execution-t7'=>'After service',
    'execution-c1'=>'After listening to the aspirations problems of customers, we will explain these issues with you.',
    'execution-c2'=>'Besides the aspirations and problems of customers. We will be check equipment-area needs cleaning 
                    to understand and problems may arise.',
    'execution-c3'=>'We will make demonstrations some places to be able professionalism of our service.',
    'execution-c4'=>'After kmowing that is required to the aspirations, we offer a maintenance plan with the most
                    appropriate, and also meet the expectations of customers on maintenance costs.',
    'execution-c5'=>'After the two sides agreed plan contents and cost, we will sign and implement contracts.',
    'execution-c6'=>'After finishing work, we will send the report to customers.',
    'execution-c7'=>'We\'ll come to check the status of work periodically. You can contact us whenever required.',

];
