<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Message Language Lines
    |--------------------------------------------------------------------------
    |   Coding : 'welcome' => 'Welcome to our application',
    |   Use it : {{ __('message.welcome')}}
    |   Display : Welcome to our application
    |
    */

    // Common Message
    'received_new_contact' => '新しい連絡先が :value つ届きました',
    'no_new_contact' => '新しい連絡先はありません',
    'incorrect_password' => '間違ったパスワード',
    'change_passpword_success' => 'パスワードを変更しました',
];
