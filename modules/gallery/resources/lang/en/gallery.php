<?php
return [
    'title' => 'Title',
    'description' => 'Description',
    'image' => 'Image',
    'link' => 'Link',
    'type' => 'Type',
    'order' => 'Order',
    'status' => 'Status',
    'active' => 'Activie',
    'choose_image' => 'Choose image',
    'upload_files' => 'Upload files',

    //Gallery
    'galleries' => 'Galleries',
    'gallery_list' => 'Gallery list',
    'add_gallery' => 'Add new gallery',
    'edit_gallery' => 'Edit gallery',
    //Banner
    'banners' => 'Banners',
    'banner_list' => 'Banner list',
    'add_banner'=> 'Add new banner',
    'edit_banner'=> 'Edit banner'
];
