<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('admin.navbar',function ($view){
            $contacts = null;
            $contacts_view = null;
            $view->with('contacts',$contacts)->with('contacts_view',$contacts_view);
        });
    }
}
