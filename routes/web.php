<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

Route::get('lang/{lang}', 'LangController@setLanguage')->name('lang');

Route::group(['middleware' => 'auth', 'prefix' => 'admin', 'namespace' => 'Admin'], function (){
    Route::get('/', 'HomeController@getIndex')->name('home');
    Route::get('/dashboard', 'HomeController@getIndex')->name('dashboard');

    //users
    Route::get('/profile', 'UserController@getUserProfile')->name('user.profile.index');
    Route::post('/profile', 'UserController@updateUserProfile')->name('user.profile.update');
    Route::post('/change-password', 'UserController@changePassword')->name('user.profile.change-password');

    Route::resource('/users', 'UserController');
    Route::resource('/roles', 'RoleController');
    Route::resource('/groups', 'GroupController');
    Route::resource('/menus', 'MenuController');
    Route::resource('/contacts', 'ContactController');
    Route::resource('/pages', 'PageController');

    Route::group(['prefix' => 'services'], function (){
        Route::get('/{slug}', 'ServiceController@index')->name('services.index');
        Route::get('/create/{type}', 'ServiceController@create')->name('services.create');
        Route::post('/store', 'ServiceController@store')->name('services.store');
        Route::get('/edit/{id}', 'ServiceController@edit')->name('services.edit');
        Route::post('/update/{id}', 'ServiceController@update')->name('services.update');
        Route::post('/destroy/{id}', 'ServiceController@destroy')->name('services.destroy');
    });

    Route::get('/about', 'AboutController@index')->name('about.index');
    Route::post('/about', 'AboutController@store')->name('about.store');

    Route::get('/activity-logs', 'ActivityController@getIndex')->name('activity.index');
    Route::post('/activity-logs/clear', 'ActivityController@getClear')->name('activity.clear');

    Route::group(['prefix' => 'setting'], function (){
        Route::get('general', 'SettingController@getGeneral')->name('admin.setting');
        Route::post('email/edit', [
            'as' => 'settings.email.edit',
            'uses' => 'SettingController@postEditEmailConfig',
        ]);
        Route::post('email/test', [
            'as' => 'settings.email.test',
            'uses' => 'SettingController@postTestEmailConfig',
        ]);

        Route::post('general/edit', [
            'as' => 'settings.general.edit',
            'uses' => 'SettingController@postEditGeneral',
        ]);
        Route::group(['prefix' => 'cache'], function () {
            Route::get('', [
                'as'         => 'system.cache',
                'uses'       => 'SettingController@getCacheManagement',
            ]);

            Route::post('clear', [
                'as'         => 'system.cache.clear',
                'uses'       => 'SettingController@postClearCache',

            ]);
        });
    });

});

Route::group(['namespace' => 'Frontend'], function (){
    Route::get('/', 'HomeController@getIndex')->name('frontend.home.index');
    Route::post('/contact-us', 'ContactController@sendContact')->name('frontend.contact.send');

    Route::get('/analytics', function(){
        $analyticsData = Analytics::fetchVisitorsAndPageViews(Spatie\Analytics\Period::days(7));
        dd($analyticsData);
    });

    
});

