<?php 

namespace Modules\Gallery\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Gallery\Repositories\Eloquents\{BannerRepository, GalleryRepository};
use Modules\Gallery\Repositories\Contracts\{IBanner, IGallery};

use File;

class MainServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/../../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'Gallery');
        $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');
        $this->loadTranslationsFrom(__DIR__ . '/../../resources/lang', 'Gallery');
    }

    public function register()
    {
        // Merge config
        $this->mergeConfigFrom(__DIR__ . '/../../config/gallery_config.php', 'gallery_config');

        // Add Provider for service container
        $this->app->bind(IBanner::class, BannerRepository::class);
        $this->app->bind(IGallery::class, GalleryRepository::class);

        // Load helper
        $this->autoload(__DIR__ . '/../../helpers');
    }

    public static function autoload($directory)
    {
        $helpers = File::glob($directory . '/*.php'); //get all files in directory
        // load file into App
        foreach ($helpers as $helper) {
            File::requireOnce($helper);
        }
    }
}