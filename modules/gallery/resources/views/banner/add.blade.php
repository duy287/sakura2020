@extends('admin.app')

@section('title', 'SKR | Banners')

@section('content')
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
        <form action="{{route('banners.store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-8">
                    <div class="card card-primary card-outline">
                        <div class="card-header"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{__('Gallery::gallery.add_banner')}}</div>
                        <div class="card-body">
                            <div class="form-group">  
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="vi-tab" data-toggle="tab" href="#vi" role="tab" aria-controls="vi" aria-selected="false">{{__('layout.vietnamese')}}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="ja-tab" data-toggle="tab" href="#ja" role="tab" aria-controls="ja" aria-selected="true">{{__('layout.japanese')}}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en" aria-selected="false">{{__('layout.english')}}</a>
                                    </li>
                                </ul>
                            
                                <div class="tab-content tabs-trans" id="myTabContent">
                                    <!--Vietnamese-->
                                    <div class="tab-pane fade show active" id="vi" role="tabpanel" aria-labelledby="vi-tab">
                                        <div class="form-group">
                                            <label>{{__('Gallery::gallery.title')}}</label>
                                            <input type="text" name="vi[title]" value="{{ old('vi.title') }}" 
                                                class="form-control @error('vi.title') is-invalid @enderror">
                                            @error('vi.title')<div class="invalid-feedback">{{ $message }}</div>@enderror
                                        </div>
                                        <div class="form-group">
                                            <label>{{__('Gallery::gallery.description')}}</label>
                                            <textarea class="form-control" rows="3" name="vi[description]">{{ old('vi.description') }}</textarea>
                                        </div>
                                    </div>
                                    <!--Japanese-->
                                    <div class="tab-pane fade" id="ja" role="tabpanel" aria-labelledby="ja-tab">
                                        <div class="form-group">
                                            <label>{{__('Gallery::gallery.title')}}</label>
                                            <input type="text" name="ja[title]" value="{{ old('ja.title') }}" 
                                                class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>{{__('Gallery::gallery.description')}}</label>
                                            <textarea class="form-control" rows="3" name="ja[description]">{{ old('ja.description') }}</textarea>
                                        </div>
                                    </div>
                                    <!--English-->
                                    <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
                                        <div class="form-group">
                                            <label>{{__('Gallery::gallery.title')}}</label>
                                            <input type="text" name="en[title]" value="{{ old('en.title') }}" 
                                                class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>{{__('Gallery::gallery.description')}}}</label>
                                            <textarea class="form-control" rows="3" name="en[description]">{{ old('en.description') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="slug">{{__('Gallery::gallery.link')}}</label>
                                <input type="text" class="form-control" name="link" value="{{ old('link') }}">
                            </div>
                            <div class="form-group">
                                <label for="type">{{__('Gallery::gallery.type')}}</label>
                                <select id="type" class="form-control" name="type">
                                    <option value="1">Banner</option>
                                    <option value="2">Slider</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="sort_order">{{__('Gallery::gallery.order')}}</label>
                                <input type="number" class="form-control" name="sort_order" min="0" value="{{ old('sort_order', 0) }}">
                            </div>
                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="gridCheck" name="status" value="1">
                                    <label class="form-check-label" for="gridCheck">Active</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-right">
                    <div class="card card-primary card-outline">
                        <div class="card-body">
                            <label for="">{{__('Gallery::gallery.choose_image')}}</label>
                            <div class="form-group">
                                <label class="upload-photo btn btn-success btn-sm" for="upload-photo">{{__('action.choose_file')}}</label>
                                <input type="file" onchange="readImage(this)" name="image" id="upload-photo" hidden/>

                                <img id="img-preview" src="" alt="" class="img-thumbnail">
                                @error('image')<div class="text-danger">{{ $message }}</div>@enderror
                            </div>
                            <hr>
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i> Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        </section>
    </div>
@endsection
