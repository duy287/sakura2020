@extends('admin.app')

@section('title', 'SKR | Setting system cache')

@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">
    <div class="setting-cache content">
        <div class="card  p-3 ">
            <div class="widget-title">
                <h4>
                    <span><i class="fas fa-sync"></i>{{__('setting.cache_cm')}}</span>
                </h4>
            </div>
            <div class="widget-body">
                <table class="table table-bordered vertical-middle table-hover">
                    <colgroup>
                        <col width="70%">
                        <col width="30%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <td>{{__('setting.cache_c')}}</td>
                        <td>
                            <button class="btn btn-danger btn-block btn-clear-cache" data-type="clear_cms_cache"
                                    data-url="{{ route('system.cache.clear') }}">
                                {{__('setting.cache_b')}}
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{__('setting.view_c')}}
                        </td>
                        <td>
                            <button class="btn btn-warning btn-block btn-clear-cache" data-type="refresh_compiled_views"
                                    data-url="{{ route('system.cache.clear') }}">
                                {{__('setting.view_b')}}
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{__('setting.config_c')}}
                        </td>
                        <td>
                            <button class="btn btn-success btn-clear-cache w-100" data-type="clear_config_cache"
                                    data-url="{{ route('system.cache.clear') }}">
                                {{__('setting.config_b')}}
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>{{__('setting.route_c')}}</td>
                        <td>
                            <button class="btn btn-success btn-clear-cache w-100" data-type="clear_route_cache"
                                    data-url="{{ route('system.cache.clear') }}">
                                {{__('setting.route_b')}}
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{__('setting.sys_c')}}
                        </td>
                        <td>
                            <button class="btn btn-success btn-clear-cache w-100" data-type="clear_log"
                                    data-url="{{ route('system.cache.clear') }}">
                                {{__('setting.sys_b')}}
                            </button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
        </div>
    </div>
@stop
