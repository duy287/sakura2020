<?php
namespace Modules\Gallery\Repositories\Eloquents;
use Modules\Gallery\Repositories\Contracts\IBanner;
use App\Repositories\Eloquent\BaseRepository;
use Modules\Gallery\Models\Banner;

class BannerRepository extends BaseRepository implements IBanner 
{
    public function model(){
        return Banner::class;
    }

    public function datatable(){
        return  datatables()->of($this->model::orderBy('created_at','desc')->get())
        ->editColumn('image', function ($item) {
            return image_thumb('banners/'.$item->image);
        })
        ->addColumn('type_name', function ($item) {
            $type_name = "Slider";
            if($item->type==1){
                $type_name = "Banner";
            }
            return $type_name;
        })
        ->editColumn('status', function ($item) {
            return status_checkbox($item);
        })
        ->rawColumns(['image', 'type_name', 'status'])
        ->make(true);
    }
}
