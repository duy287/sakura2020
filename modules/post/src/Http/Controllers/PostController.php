<?php

namespace Modules\Post\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Post\Repositories\Contracts\IPost;
use Modules\Post\Repositories\Contracts\ICategory;
use Modules\Post\Http\Requests\PostRequest;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    protected $postRepository, $cateRepository;

    public function __construct(IPost $postRepository, ICategory $cateRepository)
    {
        $this->postRepository = $postRepository;
        $this->cateRepository = $cateRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->postRepository->datatable();
        }
        return view('Post::post.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = $this->cateRepository->all();
        return view('Post::post.add', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $data = $request->except(['_token']);

        if ($request->hasFile('featured_image')) {
            $file = $request->file('featured_image');
            $data['featured_image'] = time().'_'.$file->getClientOriginalName();
            Storage::putFileAs('images/posts', $file, $data['featured_image']);
        }
        $this->postRepository->create($data);
        return redirect()->route('posts.index')->with([
            'flash_level'   => 'success',
            'flash_message' => 'Add successfully'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = $this->postRepository->find($id);
        $categories = $this->cateRepository->all();
        return view('Post::post.edit', compact('post', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, $id)
    {
        $data = $request->except(['_token', '_method']);
        $post = $this->postRepository->find($id);

        if ($request->hasFile('featured_image')) {
            //remove old file
            if ($post->featured_image) {
                try {
                    Storage::delete('images/posts/' . $post->featured_image);
                } catch (\Exception $e) {
                    return $e->getMessage();
                }
            }
            $file = $request->file('featured_image');
            $data['featured_image'] = time() . '_' . $file->getClientOriginalName();
            Storage::putFileAs('images/posts/', $file, $data['featured_image']);
        }

        $this->postRepository->update($id, $data);
        return redirect()->route('posts.index')->with([
            'flash_level'   => 'success',
            'flash_message' => 'Update successfully'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = $this->postRepository->find($id);
        //remove old file
        if ($post->featured_image) {
            try {
                Storage::delete('images/posts/' . $post->featured_image);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }
        if($this->postRepository->delete($id)){
            return response()->json([
                'row_id' => $id,
                'message' => 'Post deleted successfully',
                'status_code' => 200
            ], 200);
        }
        else{
            return response()->json([
                'row_id' => $id,
                'message' => 'Some error occured, please try again',
                'status_code' => 500
            ], 500);
        }
    }

    /**
     * Update status the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateStatus($id){
        $post = $this->postRepository->find($id);
        $status = ((int)$post->status) == 0 ? 1: 0;
        
        if($this->postRepository->update($id, ['status'=> $status])){
            return response()->json([
                'message'=> trans('action.update_success', ['m'=> 'status']),
                'status_code' => 200
            ]);
        }
        else{
            return response()->json([
                'message' => trans('action.has_error'),
                'status_code' => 500
            ]);
        }
    }
}
