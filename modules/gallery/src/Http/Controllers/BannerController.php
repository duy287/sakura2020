<?php

namespace Modules\Gallery\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Gallery\Repositories\Contracts\IBanner;
use Modules\Gallery\Http\Requests\BannerRequest;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class BannerController extends Controller
{
    protected $bannerRepository;

    public function __construct(IBanner $bannerRepository)
    {
        $this->bannerRepository = $bannerRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->bannerRepository->datatable();
        }
        return view('Gallery::banner.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Gallery::banner.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BannerRequest $request)
    {
        $data = $request->except(['_token']);

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $data['image'] = time().'_'.$file->getClientOriginalName();
            Storage::putFileAs('images/banners/', $file, $data['image']);
        }

        $this->bannerRepository->create($data);
        
        return redirect()->route('banners.index')->with([
            'flash_level'   => 'success',
            'flash_message' => trans('action.add_success', ['m'=>'Image'])
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = $this->bannerRepository->find($id);
        return view('Gallery::banner.edit', compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BannerRequest $request, $id)
    {
        $data = $request->except(['_token']);

        $banner = $this->bannerRepository->find($id);

        if ($request->hasFile('image')) {
            //remove old file
            if ($banner->image) {
                try {
                    Storage::delete('images/banners/' . $banner->image);
                } catch (\Exception $e) {
                    return $e->getMessage();
                }
            }
            $file = $request->file('image');
            $data['image'] = time() . '_' . $file->getClientOriginalName();
            Storage::putFileAs('images/banners/', $file, $data['image']);
        }

        $this->bannerRepository->update($id, $data);

        return redirect()->route('banners.index')->with([
            'flash_level'   => 'success',
            'flash_message' => trans('action.update_success', ['m'=>'Banner'])
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        if($this->bannerRepository->delete($id)){
            return response()->json([
                'row_id' => $id,
                'message' => trans('action.delete_success', ['m'=>'Banner']),
                'status_code' => 200
            ], 200);
        }
        else{
            return response()->json([
                'row_id' => $id,
                'message' => trans('action.has_error'),
                'status_code' => 500
            ], 500);
        }
    }

    /**
     * Update status the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateStatus($id){
        $banner = $this->bannerRepository->find($id);
        $status = ((int) $banner->status)=== 0 ? 1: 0;

        if($this->bannerRepository->update($id, ['status'=> $status])){
            return response()->json([
                'status' => $status,
                'message'=> trans('action.update_success', ['m'=> 'status']),
                'status_code' => 200
            ]);
        }
        else{
            return response()->json([
                'message' => trans('action.has_error'),
                'status_code' => 500
            ]);
        }
    }
}
