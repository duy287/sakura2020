@extends('admin.app')

@section('title', 'SKR | Users')

@section('content')
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-9">
                    <div class="card card-primary card-outline">
                        <div class="card-header"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{__('layout.add_user')}}</div>
                        <div class="card-body">
                            <form action="{{route('users.store')}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="name">{{__('layout.name')}}</label>
                                    <input type="text" id="name" name="name" value="{{ old('name') }}" 
                                        class="form-control @error('name') is-invalid @enderror">
                                    @error('name')<div class="invalid-feedback">{{ $message }}</div>@enderror
                                </div>
                                <div class="form-group">
                                    <label for="username">{{__('layout.username')}}</label>
                                    <input type="text" id="username" name="username" value="{{ old('username') }}" 
                                        class="form-control @error('username') is-invalid @enderror">
                                    @error('username')<div class="invalid-feedback">{{ $message }}</div>@enderror
                                </div>
                                <div class="form-group">
                                    <label for="email">{{__('layout.email')}}</label>
                                    <input type="text" id="email" name="email" value="{{ old('email') }}" 
                                        class="form-control @error('email') is-invalid @enderror">
                                    @error('email')<div class="invalid-feedback">{{ $message }}</div>@enderror
                                </div>
                                <div class="form-group">
                                    <label for="password">{{__('layout.password')}}</label>
                                    <input type="password" id="password" name="password" value="{{ old('password') }}" 
                                        class="form-control @error('password') is-invalid @enderror">
                                    @error('password')<div class="invalid-feedback">{{ $message }}</div>@enderror
                                </div>
                                <div class="form-group">
                                    <label for="password_confirmation">{{__('layout.password_confirm')}}</label>
                                    <input type="password" id="password_confirmation" name="password_confirmation" 
                                        value="{{ old('password_confirmation') }}" 
                                        class="form-control @error('password_confirmation') is-invalid @enderror">
                                    @error('password_confirmation')<div class="invalid-feedback">{{ $message }}</div>@enderror
                                </div>
                                <div class="form-group">
                                    <label for="role_id">{{__('layout.role')}}</label>
                                    <select id="role_id" class="form-control" name="role_id">
                                        <option value="">--Select role--</option>
                                        @foreach($roles as $role)
                                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="avatar">{{__('layout.avatar')}}</label>
                                    <label class="upload-photo btn btn-success btn-sm" for="upload-photo">{{__('action.choose_file')}}</label>
                                    <input type="file" onchange="readImage(this)" name="avatar" id="upload-photo" class="@error('password_confirmation') is-invalid @enderror" hidden/>
                                    <img id="img-preview" src="" alt="" class="img-thumbnail">
                                    @error('avatar')<div class="invalid-feedback">{{ $message }}</div>@enderror
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i> {{__('action.save')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
