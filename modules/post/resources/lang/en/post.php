<?php
return [
    'posts' => 'Posts',
    'post_list' => 'Post list',
    'add_post' => 'Add new post',
    'edit_post' => 'Edit post',
    'slug'  => 'Slug',
    'author'=> 'Author',
    'type' => 'Type',
    'title'=> 'Title',
    'status' => 'Status',
    'tags' => 'Tags',
    'featured_image' => 'Featured image',
    'category' => 'Category',
    'belong_to_category' => 'Belong to category',
    'active' => 'Active',
    'content' => 'Content'
];
