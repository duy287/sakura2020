@extends('admin.app')

@section('title', 'SKR | Users')

@section('content')
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-8">
                    <div class="card card-primary card-outline">
                        <div class="card-header"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{__('layout.add_menu')}}</div>
                        <div class="card-body">
                            <form action="{{route('menus.store')}}" method="POST">
                                @csrf
                                <div class="form-group">  
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="vi-tab" data-toggle="tab" href="#vi" role="tab" aria-controls="vi" aria-selected="true">{{__('layout.vietnamese')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="ja-tab" data-toggle="tab" href="#ja" role="tab" aria-controls="ja" aria-selected="false">{{__('layout.japanese')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en" aria-selected="false">{{__('layout.english')}}</a>
                                        </li>
                                    </ul>
                                
                                    <div class="tab-content tabs-trans" id="myTabContent">
                                        <div class="tab-pane fade show active" id="vi" role="tabpanel" aria-labelledby="vi-tab">
                                            <div class="form-group">
                                                <label for="name">{{__('layout.name')}}</label>
                                                <input type="text" id="name" name="vi[name]" value="{{ old('vi.name') }}" class="form-control @error('vi.name') is-invalid @enderror">
                                                @error('vi.name')<div class="invalid-feedback">{{ $message }}</div>@enderror
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="ja" role="tabpanel" aria-labelledby="ja-tab">
                                            <div class="form-group">
                                                <label for="name">{{__('layout.name')}}</label>
                                                <input type="text" id="name" name="ja[name]" value="{{ old('ja.name') }}" 
                                                    class="form-control">
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
                                            <div class="form-group">
                                                <label for="name">{{__('layout.name')}}</label>
                                                <input type="text" id="name" name="en[name]" value="{{ old('en.name') }}" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                        <label for="name">{{__('layout.slug')}}</label>
                                        <input type="text" name="slug" value="{{ old('slug') }}" onfocusout="convertToSlug($(this))" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                        <label for="name">{{__('layout.sort_order')}}</label>
                                        <input type="number" name="sort_order" value="{{ old('sort_order', 0) }}" min="0" class="form-control">
                                    </div>
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i> {{__('action.save')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
