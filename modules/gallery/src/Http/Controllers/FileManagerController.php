<?php

namespace Modules\Gallery\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FileManagerController extends Controller
{
    public function getIndex(){
        return view('Gallery::file_manager.index');
    }
}
