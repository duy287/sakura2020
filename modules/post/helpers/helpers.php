<?php 
use Carbon\Carbon;
if (!function_exists('format_date')) {
    /**
     * @param $time
     * @param string $format
     * @return mixed
     */
    function format_date($time, $format = 'Y-m-d')
    {
        if (empty($time)) {
            return "";
        }
        return Carbon::parse($time)->format($format);
    }
}