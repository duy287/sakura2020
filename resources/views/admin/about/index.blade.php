@extends('admin.app')

@section('title', 'SKR | Setting')

@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row content">
                <div class="col-md-8">
                    <form role="form" method="POST" action="{{ route('about.store') }}">
                        @csrf
                        <div class="card">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title"><i class="fa fa-cog" aria-hidden="true"></i> {{__('setting.about_us')}}</h4>
                                <div class="card-category">{{__('setting.about_info')}}</div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">{{__('setting.company_name')}}</label>
                                            <input type="text" class="form-control" name="company_name" value="{{ setting('company_name')}}">
                                        </div>
                                        <div class="form-group">
                                            <label class="bmd-label-floating">{{__('setting.head_office_address')}}</label>
                                            <input type="text" class="form-control" name="head_office_address" value="{{ setting('head_office_address')}}">
                                        </div>
                                        <div class="form-group">
                                            <label class="bmd-label-floating">{{__('setting.branch_address')}}</label>
                                            <input type="text" class="form-control" name="branch_address" value="{{ setting('branch_address')}}">
                                        </div>
                                        <div class="form-group">
                                            <label class="bmd-label-floating">{{__('setting.tel')}}</label>
                                            <input type="text" class="form-control" name="tel" value="{{ setting('tel')}}">
                                        </div>
                                        <div class="form-group">
                                            <label class="bmd-label-floating">{{__('setting.fax')}}</label>
                                            <input type="text" class="form-control" name="fax" value="{{ setting('fax')}}">
                                        </div>
                                        <div class="form-group">
                                            <label class="bmd-label-floating">{{__('setting.hotline')}}</label>
                                            <input type="text" class="form-control" name="hotline" value="{{ setting('hotline')}}">
                                        </div>
                                        <div class="form-group">
                                            <label class="bmd-label-floating">{{__('setting.contact_email')}}</label>
                                            <input type="email" class="form-control" name="contact_email" value="{{ setting('contact_email')}}">
                                        </div>
                                        <div class="text-right">
                                            <button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i> {{__('action.save')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
