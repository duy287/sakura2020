<?php
namespace App\Repositories\Eloquent;
use App\Models\Group;
use App\Repositories\Contracts\IGroup;

class GroupRepository extends BaseRepository implements IGroup 
{
    public function model(){
        return Group::class ;
    }
}
