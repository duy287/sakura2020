<?php

namespace App\Providers;
use App\Facades\SettingFacade;
use App\Supports\SettingsManager;
use App\Supports\SettingStore;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use App\Repositories\Contracts\{IUser, IRole, IGroup, IMenu, IActivity, IContact, IPage, IService};
use App\Repositories\Eloquent\{UserRepository, RoleRepository, GroupRepository,
    MenuRepository, ActivityRepository, ContactRepository, PageRepository, ServiceRepository};
use Illuminate\Support\Facades\View;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SettingStore::class, function (Application $app) {
            return $app->make(SettingsManager::class)->driver();
        });
        AliasLoader::getInstance()->alias('Setting', SettingFacade::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(IUser::class, UserRepository::class);
        $this->app->bind(IRole::class, RoleRepository::class);
        $this->app->bind(IGroup::class, GroupRepository::class);
        $this->app->bind(IMenu::class, MenuRepository::class);
        $this->app->bind(IActivity::class, ActivityRepository::class);
        $this->app->bind(IContact::class, ContactRepository::class);
        $this->app->bind(IPage::class, PageRepository::class);
        $this->app->bind(IService::class, ServiceRepository::class);
    }
}
