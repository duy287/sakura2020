<?php

namespace Modules\Gallery\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Modules\Gallery\Models\Banner;

class BannerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
        $id = $this->route('banner');
        if($id){
            $rules = [
                'vi.title' => 'required'
            ]; 
            $gallery = Banner::find($id);
            if ($gallery->notHavingImageInDb()){
                $rules['image'] = 'required';
            }
        }
        else{
            $rules =  [
                'vi.title' => 'required',
                'image'=> 'required',
            ];    
        }
        return $rules;
    }

    public function attributes()
    {
        return [
            'vi.title' => trans('Gallery::gallery.title'),
            'image' => trans('Gallery::gallery.image')
        ];
    }
}
