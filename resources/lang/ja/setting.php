<?php
return [
    'setting_mail' => 'メールテンプレートの設定',
    'setting_des' => 'HTMLとシステム変数を使用したメールテンプレート。',
    'driver' => '運転者',
    'port' => '港',
    'name' => '名前',
    'host' => 'ホスト',
    'us' => 'ユーザー名',
    'pass' => 'パスワード',
    'encryption' => '暗号化',
    'sender_name' => '送信者名',
    'sender_mail' => '送信者のメール',
    'send' => 'テストメールを送信する',
    'save' => '設定を保存',
    'send_des' => 'テストメールを送信するには、メールを送信するように構成が更新されていることを確認してください。',
    'notification'       => '設定を保存しました。',
    'err'=>'メール送信テストエラー。メール設定を確認してください。',
    'general'        => '一般的な設定',
    'logo'        => '管理ロゴ',
    'favicon'        => '管理者ファビコン',
    'choose'        => 'ファイルを選ぶ',
    'title'        => '管理職',
    'change'        => '変化する',
    'exchange-rate'=> '為替レート表',
    'cache_cm'=>'キャッシュコマンドのクリア',
    'cache_c'        => 'CMSキャッシングのクリア：データベースキャッシング、静的ブロック...データの更新後に変更が表示されない場合は、このコマンドを実行します。',
    'view_c'        => 'コンパイル済みビューをクリアして、ビューを最新にします。',
    'config_c'        => '実稼働環境で何かを変更した場合、構成キャッシングをリフレッシュする必要があるかもしれません。',
    'route_c'        => 'キャッシュルーティングをクリアします。',
    'sys_c'        => 'システムログファイルをクリアする',
    'cache_b'        => 'すべてのシステムキャッシュをクリアする',
    'view_b'        => 'コンパイルされたビューを更新する',
    'config_b'        => '構成ルートをクリア',
    'route_b'        => 'ルートキャッシュをクリア',
    'sys_b'        => 'ログをクリア',
    'cache_msg'        => 'キャッシュ成功をクリア',
    'about'=>'私たちに関しては'
];
