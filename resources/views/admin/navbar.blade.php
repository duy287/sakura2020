<section class="main-header">
    <a href="{{url('admin')}}" class="logo" >
        <span class="logo-mini"><b>P</b>S</span>
        <img src="{{asset('img/page/logo.png')}}" alt="logo" class="logo-default" style="max-width: 120px;margin-right: 20px">
    </a>
    <nav class="main-header navbar navbar-expand border-bottom navbar-dark">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="{{url('/admin')}}" class="nav-link">Home</a>
            </li>
            <?php $segment = count(Request::segments()) >1 ? str_replace('-', '_', Request::segments()[1]) : null?>
            @if($segment)
                <li class="nav-item nav-link">/</li>
                <li class="nav-item active">
                    <a href="{{ url("admin/$segment") }}" class="nav-link">{{__("layout.$segment")}}</a>
                </li>
            @endif
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class=" dropdown-header-name text-white" style="padding-right: 10px;text-decoration: none;" href="{{url('/')}}"
                   target="_blank"><i class="fa fa-globe"></i> <span class="d-none d-sm-inline">{{__('layout.view_website')}} </span> </a>
            </li>
            <li class="nav-item">
                <a href="{{route('logout')}}" class="text-white" style="margin: 0px 5px;text-decoration: none;" id="btn-logout">
                    <i class="fa fa-sign-out"></i>
                    <span>Logout<span>
                </a>
            </li>
            <li class="nav-item">
                <li class="dropdown dropdown-inbox " id="header_inbox_bar">
                    <?php $notify_count = Auth::user()->unreadNotifications->count()?>
                    <a href="#" class="dropdown-toggle dropdown-header-name text-white" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-envelope"></i>
                        @if($notify_count>0)
                            <span class="badge badge-default"> {{ $notify_count }} </span>
                        @endif
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li>
                            <a href="{{ route('contacts.index') }}" class="dropdown-item">
                                @if($notify_count==0)
                                    {{__('message.no_new_contact')}}
                                @else
                                    {{__('message.received_new_contact', ['value'=>Auth::user()->unreadNotifications->count()]) }}
                                @endif
                            </a>
                        </li>
                    </ul>
                </li>
            </li>
            <li class="language dropdown nav-item">
                <a href="javascript:;" class="dropdown-header-name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src="{{asset('/img/lang/' . App::getLocale() . '.png')}}">
                    <span class="d-none d-sm-inline">{{__('layout.'.App::getLocale())}}</span>
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-right icons-right language-admin">
                    @foreach(config('translatable.locales') as $lang)
                        @if(App::getLocale()!= $lang)
                            <li>
                                <a href="{{ route('lang', ['lang' => $lang]) }}">
                                    <img src="{{asset('/img/lang/'.$lang.'.png')}}"><span>{{__('layout.'.$lang)}}</span>
                                </a>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </li>
        </ul>
    </nav>
</section>
