<?php
namespace App\Repositories\Eloquent;
use App\Models\Contact;
use App\Repositories\Contracts\IContact;
use Illuminate\Support\Str;

class ContactRepository extends BaseRepository implements IContact 
{
    public function model(){
        return Contact::class;
    }

    public function datatable(){
        return datatables()->of($this->model::orderBy('created_at','desc')->get())
            ->addIndexColumn()
            ->editColumn('status', function ($item) {
                if($item->status==1){
                    return "<span class='label-success p-1'>Success</span>";
                }
                else{
                    return "<span class='label-primary p-1'>Pending</span>";
                }
            })
            ->editColumn('message', function ($item) {
                return Str::limit($item->message, 60);
            })
            ->rawColumns(['status'])
            ->make(true);
    }
}
