<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Contracts\IActivity;

class ActivityController extends Controller
{
    protected $activityRepository;

    public function __construct(IActivity $activityRepository)
    {
        $this->activityRepository = $activityRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
        if (request()->ajax()) {
            return $this->activityRepository->datatable($request);
        }
        return View('admin.activity.index');
    }

    public function getClear(Request $request){
        if($this->activityRepository->clear($request)){
            return response('Success', 200);
        }
        return response('Error', 500);
    }
}
