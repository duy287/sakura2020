@extends('admin.app')

@section('title', 'SKR | Service')

@section('content')
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-8">
                    <div class="card card-primary card-outline">
                        <div class="card-header"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{__("layout.$trans_key")}}</div>
                        <div class="card-body">
                            <form action="{{route('services.store')}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">  
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="vi-tab" data-toggle="tab" href="#vi" role="tab" aria-controls="vi" aria-selected="false">{{__('layout.vietnamese')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="ja-tab" data-toggle="tab" href="#ja" role="tab" aria-controls="ja" aria-selected="true">{{__('layout.japanese')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en" aria-selected="false">{{__('layout.english')}}</a>
                                        </li>
                                    </ul>
                                
                                    <div class="tab-content tabs-trans" id="myTabContent">
                                        <div class="tab-pane fade show active" id="vi" role="tabpanel" aria-labelledby="vi-tab">
                                            <div class="form-group">
                                                <label for="name">{{__('layout.name')}}</label>
                                                <input type="text" name="vi[title]" value="{{ old('vi.title') }}" class="form-control
                                                @error('vi.title') is-invalid @enderror">
                                                @error('vi.title')<div class="invalid-feedback">{{ $message }}</div>@enderror
                                            </div>
                                            <div class="form-group">
                                                <label>{{__('layout.description')}}</label>
                                                <textarea class="form-control @error('vi.description') is-invalid @enderror" rows="3" 
                                                name="vi[description]">{{ old('vi.description') }}</textarea>
                                                @error('vi.description')<div class="invalid-feedback">{{ $message }}</div>@enderror
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="ja" role="tabpanel" aria-labelledby="ja-tab">
                                            <div class="form-group">
                                                <label for="name">{{__('layout.title')}}</label>
                                                <input type="text" name="ja[title]" value="{{ old('ja.title') }}" 
                                                    class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>{{__('layout.description')}}</label>
                                                <textarea class="form-control" rows="3" name="ja[description]">{{ old('ja.description') }}</textarea>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
                                            <div class="form-group">
                                                <label for="name">{{__('layout.title')}}</label>
                                                <input type="text" name="en[title]" value="{{ old('en.title') }}" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>{{__('layout.description')}}</label>
                                                <textarea class="form-control" rows="3" name="en[description]">{{ old('en.description') }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name">{{__('layout.sort_order')}}</label>
                                    <input type="number" min="0" name="sort_order" value="{{ old('sort_order', 0) }}" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="type">{{__('layout.type')}}</label>
                                    <select id="type" class="form-control" name="type" disabled>
                                        <option value="1" {{$type==1? 'selected':''}}>{{__('layout.our_services')}}</option>
                                        <option value="2" {{$type==2? 'selected':''}}>{{__('layout.execution_order')}}</option>
                                        <option value="3" {{$type==3? 'selected':''}}>{{__('layout.bussiness_outline')}}</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="avatar">{{__('layout.image')}}</label>
                                    <div>
                                        <label class="upload-photo btn btn-success btn-sm" for="upload-photo">{{__('action.choose_file')}}</label>
                                        <input type="file" onchange="readImage(this)" name="image" id="upload-photo" hidden/>    
                                    </div>
                                    <img id="img-preview" src="" alt="" class="img-thumbnail">
                                    @error('image')<div class="text-danger">{{ $message }}</div>@enderror
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i> {{__('action.save')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection