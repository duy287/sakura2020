<?php

use Facade\FlareClient\View;
use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Modules\Gallery\Http\Controllers', 'middleware' => 'web'], function () {
    Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
        Route::post('/banners/status/{id}', 'BannerController@updateStatus')->name('admin.banners.status');
        Route::resource('/banners', 'BannerController');
        Route::resource('/galleries', 'GalleryController');
        Route::get('/file-manager', 'FileManagerController@getIndex')->name('file_manager');
    });
});