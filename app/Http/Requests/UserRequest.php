<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Validator;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user_id = $this->route('user');
        if($user_id){
            $rules = [
                'name' =>'required',
                'username' => 'required|min:4|unique:users,username,'.$user_id,
                'email' => 'required|email|unique:users,email,'.$user_id,
                'avatar'=> 'image',
            ];
        }
        else{
            $rules = [
                'name' =>'required',
                'username'=>"required|min:4|unique:users,username",
                'email' => 'required|email|unique:users,email',
                'password' => 'required|min:6',
                'password_confirmation' => 'required|same:password',
                'avatar'=> 'image',
            ];
        }

        return $rules;
    }
}
