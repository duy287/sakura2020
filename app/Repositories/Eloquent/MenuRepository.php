<?php
namespace App\Repositories\Eloquent;
use App\Models\Menu;
use App\Repositories\Contracts\IMenu;

class MenuRepository extends BaseRepository implements IMenu 
{
    public function model(){
        return Menu::class ;
    }
}
