@extends('admin.app')

@section('title', 'SKR | Service')
@section('content')
<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <span class="lead"><i class="fa fa-list-ul" aria-hidden="true"></i> 
                            {{__("layout.$trans_key")}}
                        </span>
                        <a href="{{ route('services.create', ['type'=> $type]) }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i>
                            {{__('action.create')}}
                        </a>
                    </div>
                    <div class="card-body ">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{__('layout.title')}}</th>
                                        <th>{{__('layout.image')}}</th>
                                        <th style="width:40%">{{__('layout.description')}}</th>                                       
                                        <th>{{__('layout.action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($items as $item)
                                    <tr>
                                        <td>{{$item->sort_order}}</td>
                                        <td>{{$item->title}}</td>
                                        <td>
                                            <img src="{{ asset("storage/images/services/$item->image") }}" alt="" class="img" style="width:60px">
                                        </td>
                                        <td>{{$item->description}}</td>
                                        <td>
                                            <a class="btn btn-primary btn-sm" href="{{ route('services.edit', $item->id) }}">
                                                <span class="fa fa-edit"></span>
                                            </a>
                                            <button class="btn btn-sm btn-danger" onclick = "checkItemExisted({{$item->id}})" id="row_{{$item->id}}" data-toggle="modal" data-target="#confirmDeleteItem">
                                                <span class="fa fa-trash"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="confirmDeleteItem" tabindex="-1" role="dialog" aria-labelledby="modelLabel"
          aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    {{__('action.delete_confirm')}}
                </div>
                <div id="confirmMessage" class="modal-body">
                    {{__('action.delete_sure', ['obj'=>'Service'])}}
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnConfirmDelete" class="btn btn-danger btn-ok"
                            onclick="deleteItem($(this).val())">
                        {{__('action.delete')}}
                    </button>
                    <button type="button" id="confirmCancel" class="btn btn-secondary" data-dismiss="modal"
                            data-toggle="modal" data-target="#confirmDeleteUser">
                        {{__('action.cancel')}}
                    </button>
                </div>
            </div>
        </div>
    </div>
  </section>
</div>
@endsection

@push('page-scripts')
<script>
    function checkItemExisted(id) {
        $('#btnConfirmDelete').val(id);
    }

    function deleteItem(id) {
        let delete_URL = "{{ url('admin/services/destroy') }}/" + id;
        let request = $.ajax({
            url: delete_URL,
            method: "POST",
        }).done(function (data) {
            $("#confirmDeleteItem").modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            
            if(data.status_code == 500){
                iziToast.success({
                    title: 'Error',
                    message: data.message,
                    position: 'topRight'
                });
            }
            else {
                $(`#row_${data.row_id}`).closest("tr").remove();
                iziToast.success({
                    title: 'Message',
                    message: data.message,
                    position: 'topRight'
                });
            }
        });
    }
</script>
@endpush

