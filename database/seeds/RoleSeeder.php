<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name'        => 'Admin',
                'description' => 'Has full permissions',
                'permission'  => json_encode([
                    'user_create',
                    'user_edit',
                    'user_delete',
                    'role_create',
                    'role_edit',
                    'role_delete',
                    'group_create',
                    'group_edit',
                    'group_delete',
                ]),
                'created_by'  => 'Developer'
            ],
        ];
        Role::insert($data);
    }
}
