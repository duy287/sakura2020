<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Contracts\IUser;
use App\Repositories\Contracts\IRole;
use App\Http\Requests\UserRequest;
use App\Http\Requests\ProfileRequest;
use Illuminate\Support\Facades\Storage;
use Auth;
use Hash;

class UserController extends Controller
{
    protected $userRepository, $roleRepository;

    public function __construct(IUser $userRepository, IRole $roleRepository)
    {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->userRepository->datatable();
        }
        return View('admin.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = $this->roleRepository->all();
        return view('admin.user.add', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $data = $request->except(['_token']);
        $data['password'] = bcrypt($data['password']);
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $data['avatar'] = time().'_'.$file->getClientOriginalName();
            $file->move('uploads/images/avatars/', $data['avatar']);
        }

        $this->userRepository->create($data);
        return redirect()->route('users.index')->with([
            'flash_level'   => 'success',
            'flash_message' => trans('action.add_success', ['obj'=> 'User'])
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->userRepository->find($id);
        $roles = $this->roleRepository->all();
        return view('admin.user.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $data = $request->except(['_token']);
        $user = $this->userRepository->find($id);
        
        if ($request->hasFile('avatar')) {
            //remove old file
            if($user->avatar){
                unlink("uploads/images/avatars/".$user->avatar); 
            }
            $file = $request->file('avatar');
            $data['avatar'] = time().'_'.$file->getClientOriginalName();
            $file->move('uploads/images/avatars/', $data['avatar']);
        }

        $this->userRepository->update($id, $data);
        return redirect()->route('users.index')->with([
            'flash_level'   => 'success',
            'flash_message' => trans('action.update_success', ['obj'=> 'User'])
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($this->userRepository->delete($id)){
            return response()->json([
                'row_id' => $id,
                'message' => trans('action.delete_success', ['obj'=> 'User']),
                'status_code' => 200
            ], 200);
        }
        else{
            return response()->json([
                'row_id' => $id,
                'message' => trans('action.has_error'),
                'status_code' => 500
            ], 500);
        }
    }

    public function getUserProfile(){
        $user = Auth::user();
        $roles = $this->roleRepository->all();
        return view('admin.user.profile', compact('user', 'roles'));
    }

    public function updateUserProfile(ProfileRequest $request){
        $data = $request->except(['_token']);

        $user = $this->userRepository->find(Auth::id());
        
        if ($request->hasFile('avatar')) {
            //remove old file
            if ($user->avatar) {
                try {
                    Storage::delete('images/' . $user->avatar);
                } catch (\Exception $e) {
                    return $e->getMessage(); 
                }
            }
            $file = $request->file('avatar');
            $data['avatar'] = time() . '_' . $file->getClientOriginalName();
            Storage::putFileAs('images', $file, $data['avatar']);
        }

        $this->userRepository->update(Auth::id(), $data);
        return redirect()->route('user.profile.index')->with([
            'flash_level'   => 'success',
            'flash_message' => trans('action.update_success', ['obj'=> 'Profile'])
        ]);
    }

    public function changePassword(Request $request){
        $validatedData = $request->validate([
            'old_password' => 'required',
            'new_password' => 'required|min:6',
            'password_confirmation' => 'required|same:new_password',
        ]);

        if (!Hash::check($request->old_password, Auth::user()->password)){
            return redirect()->route('user.profile.index')->with([
                'flash_level'   => 'error',
                'flash_message' => trans('message.incorrect_password')
            ]);
        }
        $this->userRepository->update(Auth::id(), [
            'password' => bcrypt($request->new_password)
        ]);

        return redirect()->route('user.profile.index')->with([
            'flash_level'   => 'success',
            'flash_message' => trans('message.change_passpword_success')
        ]);
    }

}
