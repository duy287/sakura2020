<?php
namespace Modules\Gallery\Repositories\Eloquents;
use Modules\Gallery\Repositories\Contracts\IGallery;
use App\Repositories\Eloquent\BaseRepository;
use Modules\Gallery\Models\Gallery;

class GalleryRepository extends BaseRepository implements IGallery 
{
    public function model(){
        return Gallery::class;
    }
}
