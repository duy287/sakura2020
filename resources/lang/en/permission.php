<?php 
return [
    //Contact
    'contact' => 'Contact',
    'view_contacts' => 'View contacts',
    'delete_contact' => 'Delete contact',

    //User
    'user' => 'User',
    'view_users' => 'View users',
    'create_user' => 'Create user',
    'edit_user' => 'Edit user',
    'delete_user' => 'Delete user',

    //Role
    'role' => 'Role',
    'view_roles' => 'View roles',
    'create_role' => 'Create role',
    'edit_role' => 'Edit role',
    'delete_role' => 'Delete role',

    //Group
    'group' => 'Group',
    'view_groups' => 'View groups',
    'create_group' => 'Create group',
    'edit_group' => 'Edit group',
    'delete_group' => 'Delete group',

    //Menu
    'menu' => 'Menu',
    'view_menus' => 'View menus',
    'create_menu' => 'Create menu',
    'edit_menu' => 'Edit menu',
    'delete_menu' => 'Delete menu',

    //Category
    'category' => 'Category',
    'view_categories' => 'View categories',
    'create_category' => 'Create category',
    'edit_category' => 'Edit category',
    'delete_category' => 'Delete category',

    //Post
    'post' => 'Post',
    'view_posts' => 'View posts',
    'create_post' => 'Create post',
    'edit_post' => 'Edit post',
    'delete_post' => 'Delete post',

    //Gallery
    'gallery' => 'Gallery',
    'view_galleries' => 'View galleries',
    'create_gallery' => 'Create gallery',
    'edit_gallery' => 'Edit gallery',
    'delete_gallery' => 'Delete gallery',

    //Page
    'page' => 'Page',
    'view_pages' => 'View pages',
    'create_page' => 'Create page',
    'edit_page' => 'Edit page',
    'delete_page' => 'Delete page',
];