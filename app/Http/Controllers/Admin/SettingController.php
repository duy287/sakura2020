<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Supports\ClearLogCommand;
use App\Supports\SettingStore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class SettingController extends Controller
{
    public function getGeneral(){
        $mail_cf = Setting::pluck('value', 'key')->all();


        return view('admin.setting.setting', [
            'mail_cf' => $mail_cf,
        ]);
    }

    public function postEditEmailConfig (Request $request, SettingStore $setting){
        foreach ($request->except(['_token']) as $setting_key => $setting_value) {
            $setting->set($setting_key, $setting_value);
        }
        $setting->save();
        return redirect()->back()->with([
            'flash_level' => 'success',
            'flash_message' => trans('setting.notification')
        ]);
    }

    public function postTestEmailConfig(Request $request)
    {
        try {
            Mail::send('admin.setting.test_mail', ['user' => $request], function ($m) use ($request) {
                $m->to($request->email_test)->subject('Test Mail Success!');
            });
            return redirect()->back()->with([
                'flash_level' => 'success',
                'flash_message' => trans('setting.mail_msg')
            ]);
        }catch (\Exception $e){
            return redirect()->back()->with([
                'flash_level' => 'error',
                'flash_message' => trans('setting.mail_err')
            ]);
        }
    }

    public function postEditGeneral(Request $request,Setting $settings, SettingStore $setting){
        $file = $request->file();

        $data = $settings->pluck('value', 'key')->all();
        $this->saveImage($file ,$data ,$setting);
        foreach ($request->except(['_token','admin_logo','admin_favicon']) as $setting_key => $setting_value) {
            $setting->set($setting_key, $setting_value);
        }


        $setting->save();;
        return redirect()->back()->with([
            'flash_level' => 'success',
            'flash_message' => trans('setting.notification')
        ]);
    }

    public function saveImage($request,$settings,SettingStore $setting)
    {
        foreach ($request as $setting_key => $setting_value) {
            if ($request[$setting_key]) {
                //Delete file old

                if(isset( $settings[$setting_key])){
                    if (Storage::disk('public')->exists('uploads/images/', $settings[$setting_key])) {
                        File::delete('uploads/images/' . $settings[$setting_key]);
                    }
                }
                // upload file
                $avatar = Str::uuid() . "." . $request[$setting_key]->getClientOriginalExtension();
                Image::make($request[$setting_key])->save(public_path('uploads/images/') . $avatar);
                $setting->set($setting_key, $avatar);
            }
        }
    }

    public function getCacheManagement()
    {
        return view('admin.setting.cache');
    }


    public function postClearCache(Request $request, ClearLogCommand $clearLogCommand)
    {

        if (function_exists('proc_open')) {
            switch ($request->input('type')) {
                case 'clear_cms_cache':
                    execute_command('cache:clear');
                    break;
                case 'refresh_compiled_views':
                    execute_command('view:clear');
                    break;
                case 'clear_config_cache':
                    execute_command('config:clear');
                    break;
                case 'clear_route_cache':
                    execute_command('route:clear');
                    break;
                case 'clear_log':
                    execute_command($clearLogCommand->getName());
                    break;
            }
        }

        return response()->json([
            'success_msg' =>  trans('setting.cache_msg')
        ], 200);

    }
}
