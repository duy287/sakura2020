@extends('admin.app')

@section('title', 'SKR | User')
@section('content')
<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <span class="lead"><i class="fa fa-list-ul" aria-hidden="true"></i> {{__('Gallery::gallery.gallery_list')}}</span>
                        <a href="{{ route('banners.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i>
                            {{__('action.create')}}
                        </a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>{{__('Gallery::gallery.image')}}</th>
                                        <th>{{__('Gallery::gallery.title')}}</th>
                                        <th>{{__('Gallery::gallery.type')}}</th>
                                        <th>{{__('Gallery::gallery.order')}}</th>
                                        <th>{{__('Gallery::gallery.active')}}</th>
                                        <th>{{__('layout.action')}}</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="confirmDeleteItem" tabindex="-1" role="dialog" aria-labelledby="modelLabel"
          aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    {{__('action.delete_confirm')}}
                </div>
                <div id="confirmMessage" class="modal-body">
                    {{__('action.delete_sure', ['obj'=>'Image'])}}
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnConfirmDelete" class="btn btn-danger btn-ok"
                            onclick="deleteItem($(this).val())">
                        {{__('action.delete')}}
                    </button>
                    <button type="button" id="confirmCancel" class="btn btn-secondary" data-dismiss="modal"
                            data-toggle="modal" data-target="#confirmDeleteUser">
                        {{__('action.cancel')}}
                    </button>
                </div>
            </div>
        </div>
    </div>
  </section>
</div>
@endsection

@push('page-scripts')
<script>
    $('#dataTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('banners.index') }}",
        columns: [
            { data: 'image', name: 'image'},
            { data: 'title', name: 'title'},
            { data: 'type_name', name: 'type_name'},
            { data: 'sort_order', name: 'sort_order'},
            { data: 'status', name: 'status'},
        ],
        columnDefs: [{
            "targets": 5, 
            "data": null,
            "render": function (data) {
                edit_url = "{{ url('admin/banners') }}/" + data.id + "/edit";
                delete_event = ' onclick = checkItemExisted(' + data.id + ') ';
                return '<a class="btn btn-primary btn-sm" href=' + edit_url + '><span class="fa fa-edit"></span></a> ' +
                '<button class="btn btn-sm btn-danger" ' + delete_event + 'id="row_' + data.id + '" data-toggle="modal" data-target="#confirmDeleteItem" >' +
                '<span class="fa fa-trash" ></span></button> ';
            }
        }],
    });

    function checkItemExisted(id) {
        $('#btnConfirmDelete').val(id);
    }

    function deleteItem(id) {
        let delete_URL = "{{ url('admin/banners') }}/" + id;
        let request = $.ajax({
            url: delete_URL,
            type: "DELETE"
        }).done(function (data) {
            $("#confirmDeleteItem").modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            if(data.status_code == 500){
                iziToast.success({
                    title: 'Error',
                    message: data.message,
                    position: 'topRight'
                });
            }
            else {
                $(`#row_${data.row_id}`).closest("tr").remove();
                iziToast.success({
                    title: 'Message',
                    message: data.message,
                    position: 'topRight'
                });
            }
        });
    }

    function changeStatus(id){
        ajax_URL = "{{ url('admin/banners/status') }}/" + id;
        let request = $.ajax({
            url: ajax_URL,
            method: "POST"
        }).done(function (data) {
            if(data.status_code == 500){
                iziToast.success({
                    title: 'Error',
                    message: data.message,
                    position: 'topRight'
                });
            }
            else {
                iziToast.success({
                    title: 'Message',
                    message: data.message,
                    position: 'topRight'
                });
            }
        });
    }
</script>
@endpush

