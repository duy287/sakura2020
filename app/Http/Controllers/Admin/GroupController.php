<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Contracts\IGroup;
use App\Repositories\Contracts\IUser;
use App\Http\Requests\GroupRequest;

class GroupController extends Controller
{
    protected $groupRepository, $userRepository;

    public function __construct(IGroup $groupRepository, IUser $userRepository)
    {
        $this->groupRepository = $groupRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->groupRepository->datatable();
        }
        return View('admin.group.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $users = $this->userRepository->all();
        return view('admin.group.add', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GroupRequest $request)
    {
        $data = $request->except(['_token']);
        $data['user_ids'] = json_encode(explode(',', $request->input('user_ids')));

        $this->groupRepository->create($data);
        return redirect()->route('groups.index')->with([
            'flash_level'   => 'success',
            'flash_message' => trans('action.add_success', ['obj'=>'group'])
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $group = $this->groupRepository->find($id);
        $users = $this->userRepository->all();
        $user_selected_ids = (array)json_decode($group->user_ids);
        return view('admin.group.edit', compact('group', 'users', 'user_selected_ids'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GroupRequest $request, $id)
    {
        $data = $request->except(['_token']);
        $data['user_ids'] = json_encode(explode(',', $request->input('user_ids')));

        $this->groupRepository->update($id, $data);
        return redirect()->route('groups.index')->with([
            'flash_level'   => 'success',
            'flash_message' =>  trans('action.update_success', ['obj'=>'group'])
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($this->groupRepository->delete($id)){
            return response()->json([
                'row_id' => $id,
                'message' =>  trans('action.delete_success', ['obj'=>'group']),
                'status_code' => 200
            ], 200);
        }
        else{
            return response()->json([
                'row_id' => $id,
                'message' => trans('action.has_error'),
                'status_code' => 500
            ], 500);
        }
    }
}
