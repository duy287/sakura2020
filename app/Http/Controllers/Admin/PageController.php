<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Contracts\IPage;
use App\Http\Requests\PageRequest;
use Illuminate\Support\Facades\Storage;

class PageController extends Controller
{
    protected $pageRepository;

    public function __construct(IPage $pageRepository)
    {
        $this->pageRepository = $pageRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->pageRepository->datatable();
        }
        return View('admin.page.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.page.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PageRequest $request)
    {
        $data = $request->except(['_token']);

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $data['image'] = time().'_'.$file->getClientOriginalName();
            Storage::putFileAs('images/pages/', $file, $data['image']);
        }
        $this->pageRepository->create($data);

        return redirect()->route('pages.index')->with([
            'flash_level'   => 'success',
            'flash_message' =>  trans('action.add_success', ['obj' => 'Page'])
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $page = $this->pageRepository->find($id);
        return view('admin.page.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PageRequest $request, $id)
    {
        $data = $request->except(['_token', '_method']);
        $page = $this->pageRepository->find($id);

        if ($request->hasFile('image')) {
            //remove old file
            if ($page->image) {
                try {
                    Storage::delete('images/pages/' . $page->image);
                } catch (\Exception $e) {
                    return $e->getMessage();
                }
            }
            $file = $request->file('image');
            $data['image'] = time() . '_' . $file->getClientOriginalName();
            Storage::putFileAs('images/pages/', $file, $data['image']);
        }

        $this->pageRepository->update($id, $data);
        return redirect()->route('pages.index')->with([
            'flash_level'   => 'success',
            'flash_message' => trans('action.update_success', ['obj' => 'Page'])
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->pageRepository->delete($id)) {
            return response()->json([
                'row_id' => $id,
                'message' => trans('action.delete_success', ['obj' => 'Page']),
                'status_code' => 200
            ], 200);
        } else {
            return response()->json([
                'row_id' => $id,
                'message' => trans('action.has_error'),
                'status_code' => 500
            ], 500);
        }
    }
}
