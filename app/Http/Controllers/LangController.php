<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;

class LangController extends Controller
{
    public function setLanguage($lang){
        Session::put('lang', $lang);
        return redirect()->back();
    }
}