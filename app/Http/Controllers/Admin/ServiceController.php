<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Contracts\IService;
use App\Http\Requests\ServiceRequest;
use Illuminate\Support\Facades\Storage;

class ServiceController extends Controller
{
    protected $serviceRepository;

    public function __construct(IService $serviceRepository)
    {
        $this->serviceRepository = $serviceRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug)
    {
        if($slug=='our-services'){
            $trans_key = 'our_services';
            $type = 1;
        }
        else if($slug=='execution-order'){
            $trans_key = 'execution_order';
            $type = 2;
        }
        else{
            $trans_key = 'bussiness_outline';
            $type = 3;
        }

        $items = $this->serviceRepository->where('type', $type)->paginate(10);
        return View('admin.service.index', compact('items', 'trans_key', 'type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create($type)
    {
        if($type==1){
            $trans_key = 'add_our_services';
        }
        else if($type==2){
            $trans_key = 'add_execution_order';
        }
        else{
            $trans_key = 'add_bussiness_outline';
        }

        return view('admin.service.add', compact('type', 'trans_key'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceRequest $request)
    {
        $data = $request->except(['_token']);

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $data['image'] = time().'_'.$file->getClientOriginalName();
            Storage::putFileAs('images/services/', $file, $data['image']);
        }
        $service = $this->serviceRepository->create($data);

        if($service->type==1){
            $slug = 'our-services';
        }
        else if($service->type==2){
            $slug = 'excution-order';
        }
        else{
            $slug = 'bussiness-outline';
        }
        
        return redirect()->route('services.index', ['slug'=> $slug])->with([
            'flash_level'   => 'success',
            'flash_message' =>  trans('action.add_success', ['m' => 'Service'])
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $item = $this->serviceRepository->find($id);
        return view('admin.service.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceRequest $request, $id)
    {
        $data = $request->except(['_token']);
        $service = $this->serviceRepository->find($id);

        if ($request->hasFile('image')) {
            //remove old file
            if ($service->image) {
                try {
                    Storage::delete('images/services/' . $service->image);
                } catch (\Exception $e) {
                    return $e->getMessage();
                }
            }
            $file = $request->file('image');
            $data['image'] = time() . '_' . $file->getClientOriginalName();
            Storage::putFileAs('images/services/', $file, $data['image']);
        }

        $this->serviceRepository->update($id, $data); 
        
        if($service->type==1){
            $slug = 'our-services';
        }
        else if($service->type==2){
            $slug = 'excution-order';
        }
        else{
            $slug = 'bussiness-outline';
        }
        
        return redirect()->route('services.index', ['slug'=> $slug])->with([
            'flash_level'   => 'success',
            'flash_message' =>  trans('action.update_success', ['m' => 'Service'])
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->serviceRepository->delete($id)) {
            return response()->json([
                'row_id' => $id,
                'message' => trans('action.delete_success', ['m' => 'Service']),
                'status_code' => 200
            ], 200);
        } else {
            return response()->json([
                'row_id' => $id,
                'message' => trans('action.has_error'),
                'status_code' => 500
            ], 500);
        }
    }
}
