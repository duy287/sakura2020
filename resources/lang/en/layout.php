<?php
return [
    // Properties
    'name' => 'Name',
    'user' => 'User',
    'username' => 'Username',
    'email' => 'Email',
    'avatar' => 'Avatar',
    'sort_order' => 'Sort order',
    'action' => 'Action',
    'slug' => 'Slug',
    'role' => 'Role',
    'description'=> 'Description',
    'per_flags' => 'Permissions',
    'ip_address' => 'IP Address',
    'history' => 'History',
    'log_time' => 'Log time',
    'more_info' => 'More info',
    'from' => 'From',
    'to' => 'To',
    'message' => 'Message',
    'status' => 'Status',
    'title' => 'Title',
    'content' => 'Content',
    'image'=> 'Image',
    'active' => 'Active',
    'type'=> 'Type',
    'date_time' => 'Date time',
    
    // Dashboard
    'dashboard' => 'Dashboard',
    'view_website' => 'View website',

    // Menu 
    'menus' => 'Menus',
    'menu_list' => 'Menu list',
    'add_menu' => 'Add new menu',
    'edit_menu' => 'Edit menu',
    
    // User
    'users' => 'Users',
    'user_list' => 'User list',
    'add_user' => 'Add new user',
    'edit_user' => 'Edit user',
    'user_profile' => 'User profile',
    'profile' => 'User profile',
    
    // Role
    'roles' => 'Roles',
    'role_list' => 'Role list',
    'add_role' => 'Add new role',
    'edit_role' => 'Edit role',

    // Group
    'groups' => 'Groups',
    'gourp_list' => 'Group list',
    'add_group' => 'Add new group',
    'edit_group' => 'Edit group',

    //Lang
    'japanese' => 'Japanese',
    'english' => 'English',
    'vietnamese' => 'Vietnamese',

    //Activity logs
    'activity_logs' => 'Activity logs',
    'clear_logs' => 'Clear the logs',

    //Contact
    'contacts' => 'Contacts',
    'contact_list' => 'Contact list',
    'contact_detail' => 'Contact detail',
    'back_to_contact_list' => 'Back to contact list',
    
    //page
    'pages' => 'Pages',
    'page_list' => 'Page list',
    'add_page' => 'Add new page',
    'edit_page' => 'Edit page',

    //Service
    'services' => 'Services',
    'our_services' => 'Our services',
    'add_our_services' => 'Add Our services',
    'execution_order' => 'execution order',
    'add_execution_order' => 'Add execution order',
    'bussiness_outline'=> 'Bussiness outline',
    'add_bussiness_outline'=> 'Add Bussiness outline',
    'edit_service' => 'Edit Service',

    //password
    'password'=>'Password',
    'password_confirm' => 'Password confirm',
    'change_password' => 'Change password',
    'old_password' => 'Old password',
    'new_password' => 'New password',

    //Breadcrumbs
    'posts' => 'Posts',
    'categories' => 'Categories',
    'galleries' => 'Galleries',
    'activity-logs' => 'Activity logs',

    //Language
    'en'=> 'English',
    'vi'=> 'Vietnamese',
    'ja'=> 'Japanese',

    //Setting
    'setting' => 'Settings',

    //About
    'about'=>'About',

    //Requirements
    'requirements' => 'Requirements',
    'execution_order' => 'Execution order',
    'bussiness_outline'=> 'Bussiness outline'
];

