<?php 
return [
    //Contact
    'contact' => '連絡先',
    'view_contacts' => '連絡先を表示',
    'delete_contact' => '連絡先を削除',

    //User
    'user' => 'ユーザー',
    'view_users' => 'ユーザーを表示',
    'create_user' => 'ユーザーを作成',
    'edit_user' => 'ユーザーを編集',
    'delete_user' => 'ユーザーを削除',

    //Role
    'role' => '役割',
    'view_roles'=>'役割を表示',
    'create_role' => '役割を作成',
    'edit_role' => '役割を編集',
    'delete_role' => '役割を削除',

    //Group
    'group' => 'グループ',
    'view_groups' => 'グループを表示',
    'create_group' => 'グループを作成します',
    'edit_group' => 'グループを編集',
    'delete_group' => 'グループを削除',

    //Menu
    'menu' => 'メニュー',
    'view_menus' => 'メニューを見る',
    'create_menu' => 'メニューを作成',
    'edit_menu' => '編集メニュー',
    'delete_menu' => '削除メニュー',

    //Category
    'category' => 'カテゴリー',
    'view_categories' => 'カテゴリを表示',
    'create_category' => 'カテゴリを作成',
    'edit_category' => 'カテゴリを編集',
    'delete_category' => 'カテゴリを削除',

    //Post
    'post' => 'ポスト',
    'view_posts' => '投稿を表示',
    'create_post' => '投稿を作成',
    'edit_post' => '投稿を編集',
    'delete_post' => '投稿を削除',

    //Gallery
    'gallery' => 'ギャラリー',
    'view_galleries' => 'ギャラリーを見る',
    'create_gallery' => 'ギャラリーを作成',
    'edit_gallery' => 'ギャラリーを編集',
    'delete_gallery' => 'ギャラリーを削除',

    //Page
    'page' => 'ページ',
    'view_pages' => 'ページを表示',
    'create_page' => 'ページを作成',
    'edit_page' => 'ページを編集',
    'delete_page' => 'ページを削除',
];