<?php
namespace App\Repositories\Eloquent;
use App\Models\Service;
use App\Repositories\Contracts\IService;
use Illuminate\Support\Str;

class ServiceRepository extends BaseRepository implements IService 
{
    public function model(){
        return Service::class;
    }

    public function datatable(){
        return datatables()->of($this->model::orderBy('created_at','desc')->get())
            ->editColumn('image', function ($item) {
                return image_thumb('services/'.$item->image);
            })
            ->editColumn('description', function ($item) {
                return Str::limit($item->description, 60);
            })
            ->rawColumns(['image'])
            ->make(true);
    }
}
