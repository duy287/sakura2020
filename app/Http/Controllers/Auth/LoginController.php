<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Models\Activity;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'a/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'username';
    }

    protected function authenticated(Request $request, $user)
    {
        Activity::saving(function (Activity $activity) {
            $activity->properties = $activity->properties->put('ip', request()->ip());
            $activity->properties = $activity->properties->put('browser', request()->header('User-Agent'));
        });
        activity()
            ->causedBy($user) //save user
            ->performedOn($user)
            ->log('Login'); //description


    }

    public function logout(Request $request)
    {
        Activity::saving(function (Activity $activity) {
            $activity->properties = $activity->properties->put("ip", request()->ip());
            $activity->properties = $activity->properties->put("browser", request()->header('User-Agent'));
        });
        activity()
            ->causedBy(Auth::user())
            ->log('Log Out');
        $this->guard()->logout();
        return redirect()->route('login');
    }
}
