@extends('admin.app')

@section('title', 'SKR | Setting')

@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row content">
                <div class="col-md-7">
                    <form role="form" method="POST" action="{{ route('settings.email.edit') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="card">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">{{__('setting.setting_mail')}}</h4>
                                <p class="card-category">{{__('setting.setting_des')}}</p>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="form-group bmd-form-group">
                                                <label class="bmd-label-floating">{{__('setting.driver')}}</label>
                                                <input type="text" class="form-control" name="smtp" value="SMTP" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-group bmd-form-group">
                                                <label class="bmd-label-floating">{{__('setting.port')}}</label>
                                                <input data-counter="10" type="number"  class="form-control" name="email_port" id="email_port"
                                                       value="{{  setting('email_port',config('mail.port')) }}" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-group bmd-form-group">
                                                <label class="bmd-label-floating">{{__('setting.host')}}</label>
                                                <input data-counter="60" type="text" class="form-control" name="email_host" id="email_host"
                                                       value="{{  setting('email_host',config('mail.host')) }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-group bmd-form-group">
                                                <label class="bmd-label-floating">{{__('setting.us')}}</label>
                                                <input data-counter="60" type="text" class="form-control" name="email_username" id="email_username"
                                                       value="{{  setting('email_username',config('mail.username')) }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-group bmd-form-group">
                                                <label class="bmd-label-floating">{{__('setting.pass')}}</label>
                                                <input data-counter="60" type="password" class="form-control" name="email_password" id="email_password"
                                                       value="{{  setting('email_password',config('mail.password')) }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-group bmd-form-group">
                                                <label for="inputState">{{__('setting.encryption')}} </label>
                                                <select id="inputState" name="email_encryption" class="form-control" style="width:50%">
                                                    <option value="tls" @if (setting('email_encryption', config('mail.encryption')) == 'tls') selected @endif>TLS</option>
                                                    <option value="ssl" @if (setting('email_encryption', config('mail.encryption')) == 'ssl') selected @endif>SSL</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-group bmd-form-group">
                                                <label class="bmd-label-floating">{{__('setting.sender_name')}} </label>
                                                <input data-counter="60" type="text" class="form-control" name="email_from_name" id="email_from_name"
                                                       value="{{  setting('email_from_name',config('mail.from.name')) }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-group bmd-form-group">
                                                <label class="bmd-label-floating">{{__('setting.sender_mail')}} </label>
                                                <input data-counter="60" type="text" class="form-control" name="email_from_address" id="email_from_address"
                                                       value="{{  setting('email_from_address',config('mail.from.address')) }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@fat">{{__('setting.send')}}</button>
                                    <button type="submit" class="btn btn-primary pull-right">{{__('action.save')}}</button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-5">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title"> {{__('setting.general')}}</h4>

                        </div>
                        <div class="card-body">
                            <form action="{{route('settings.general.edit')}}" method="POST" enctype="multipart/form-data"
                                  accept-charset="utf-8">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="form-group bmd-form-group">
                                            <div class="">{{__('setting.logo')}}</div>
                                            <div class="avatar-upload">
                                                <div class="avatar-edit">
                                                    <input type='file' id="imageUpload" accept=".png, .jpg, .jpeg" name="admin_logo"/>

                                                </div>
                                                <div class="avatar-preview">
                                                    <div id="imagePreview" style="background-image: url({{asset('/storage/avatar/'.setting('admin_logo','logo.png'))}});  border-radius: unset">
                                                    </div>

                                                </div>
                                                <label for="imageUpload" style="color: #337ab7 ">{{__('setting.choose')}}</label>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-12">

                                        <div class="form-group bmd-form-group">
                                            <div class="">{{__('setting.favicon')}} Favicon</div>
                                            <div class="avatar-upload">
                                                <div class="avatar-edit">
                                                    <input type='file' id="imageFavicon" accept=".png, .jpg, .jpeg" name="admin_favicon"/>
                                                </div>

                                                <div class="avatar-preview" >
                                                    <div id="imagePreview1" style="background-image: url({{asset('/storage/uploads/images/'.setting('admin_favicon'))}}); border-radius: unset">
                                                    </div>

                                                </div>
                                                <label for="imageFavicon" style="color: #337ab7 ">{{__('setting.choose')}}</label>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <div class="row m-t-10">
                                    <div class="col-md-12">
                                        <div class="form-group bmd-form-group">
                                            <label class="bmd-label-floating">{{__('setting.title')}} </label>
                                            <input id="admin_title" name="admin_title" type="text"
                                                   value="{{ setting('admin_title') }}" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary pull-right">{{__('setting.change')}}</button>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form role="form" method="POST" action="{{ route('settings.email.test') }}" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{__('setting.send')}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="form-group bmd-form-group">
                                <label for="recipient-name" class="col-form-label">{{__('setting.send_des')}}:</label>
                                <input type="email" class="form-control" id="recipient-name" required name="email_test">
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('action.close')}}</button>
                        <button type="submit" class="btn btn-primary">{{__('action.send')}}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
@push('page-scripts')

@endpush
