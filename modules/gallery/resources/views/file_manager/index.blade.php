@extends('admin.app')

@section('title', 'SKR | Users')

@section('content')
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div id="ckfinder-widget"></div>
                </div>
            </div>
        </form>
        </section>
    </div>
@endsection
@push('page-scripts')
<script>
    CKFinder.widget('ckfinder-widget', {
		width: '100%',
		height: 700,
        displayFoldersPanel: false,
	} );
</script>
@endpush
