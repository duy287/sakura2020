<?php
return [
    //Properties
    'name' => '名前',
    'user' => 'ユーザー',
    'username' => 'ユーザー名',
    'email' => 'Email',
    'avatar' => 'Avatar',
    'sort_order' => 'ソート順',
    'action' => 'アクション',
    'slug' => 'Slug',
    'role' => '役割',
    'description'=> '説明',
    'per_flags' => '許可',
    'ip_address' => 'IP アドレス',
    'history' => '歴史',
    'log_time' => 'ログ時間',
    'more_info' => 'より詳しい情報',
    'from' => 'から',
    'to' => 'に',
    'message' => 'メッセージ',
    'status' => '状態',
    'title' => '題名',
    'content' => 'コンテンツ',
    'image'=> '画像',
    'active' => 'アクティブ',
    'type'=> 'タイプ',
    'date_time' => '日時',

    //dashboard
    'dashboard' => 'Dashboard',
    'view_website' => 'ウェブサイトを見る',

    // Menu 
    'menus' => 'メニュー',
    'menu_list' => 'Menu リスト',
    'add_menu' => '新しいメニューを追加',
    'edit_menu' => '編集メニュー',
    
    // User
    'users' => 'ユーザー',
    'user_list' => 'ユーザーリスト',
    'add_user' => '新しいユーザーを追加',
    'edit_user' => 'ユーザーを編集',
    'user_profile' => 'ユーザープロフィール',
    'profile' => 'ユーザープロフィール',

    // Role
    'roles' => '役割',
    'role_list' => '役割リスト',
    'add_role' => '新しい役割を追加',
    'edit_role' => '役割を編集',

    // Group
    'groups' => 'グループ',
    'gourp_list' => 'グループリスト',
    'add_group' => '新しいグループを追加',
    'edit_group' => 'グループを編集',

    //Lang
    'japanese' => '日本人',
    'english' => '英語',
    'vietnamese' => 'ベトナム人',

    //Activity logs
    'activity_logs' => 'アクセス履歴',
    'clear_logs' => 'アクセス履歴をクリア',

    //Contact
    'contacts' => '連絡先',
    'contact_list' => '連絡先リスト',
    'contact_detail' => '連絡先の詳細',
    'back_to_contact_list' => '連絡先リストに戻る',

    //page
    'pages' => 'ページ',
    'page_list' => 'ページ一覧',
    'add_page' => '新しいページを追加',
    'edit_page' => 'ページを編集',

    //Service
    'services' => 'サービス',
    'our_services' => '当社のサービス',
    'execution_order' => '実行順序',
    'bussiness_outline'=>'事業概要',
    'service_list' => 'サービスリスト',
    'add_service' => '新しいサービスを追加する',
    'edit_service' => 'サービス編集',

    //Password
    'password'=>'パスワード',
    'password_confirm' => 'パスワード確認',
    'change_password' => 'パスワードを変更する',
    'old_password' => '以前のパスワード',
    'new_password' => '新しいパスワード',

    //Modules
    'posts' => 'ポスト',
    'categories' => 'カテゴリー',
    'galleries' => 'ギャラリー',
    'activity-logs' => 'アクセス履歴',

    //Language
    'en'=> '英語',
    'vi'=> 'ベトナム語',
    'ja'=> '日本語',

    //About
    'about'=>'私たちに関しては',

    //Requirements
    'requirements' => '要件',
    'execution_order' => '実行順序',
    'bussiness_outline'=> '事業概要'
];

