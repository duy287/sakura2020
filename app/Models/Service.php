<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Service extends Model
{
    use Translatable;
    public $translatedAttributes = ['title', 'description'];
    protected $fillable = ['image', 'type', 'sort_order'];

    public function notHavingImageInDb(){
        return (empty($this->image)) ? true:false;
    }
}
