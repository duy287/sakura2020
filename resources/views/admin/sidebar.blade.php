<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('storage/images/'.Auth::user()->avatar)}}" class="img float-left" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><a href="{{ route('user.profile.index') }}">{{ Auth::user()->name }}</a></p>
                <a href="{{ route('user.profile.index') }}"><i class="fa fa-circle text-success"></i>
                    Online
                </a>
            </div>
        </div>
        <ul class="sidebar-menu" data-widget="tree">
            <li class="treeview">
                <a href="{{ url('admin/dashboard') }}">
                    <i class="fa fa-dashboard"></i>
                    <span>{{__('layout.dashboard')}}</span>
                </a>
            </li>
            <li class="treeview">
                <a href="{{ url('admin/contacts') }}">
                    <i class="fa fa-envelope"></i>
                    <span>{{__('layout.contacts')}}</span>
                </a>
            </li>
            <li class="treeview">
                <a href="{{ url('admin/about') }}">
                    <i class="fa fa-address-book" aria-hidden="true"></i>
                    <span>{{__('setting.about')}}</span>
                </a>
            </li>
            <li class="treeview">
                <a href="{{ url('admin/menus') }}">
                    <i class="fa fa-indent"></i>
                    <span>{{__('layout.menus')}}</span>
                </a>
            </li>
            <li class="treeview">
                <a href="{{ url('admin/posts') }}">
                    <i class="fa fa-newspaper-o"></i>
                    <span>{{__('Post::post.posts')}}<span>
                    <i class="fa fa-angle-double-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('admin/posts') }}">{{__('Post::post.posts')}}</a></li>
                    <li><a href="{{ url('admin/categories') }}">{{__('Post::category.categories')}}</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="{{ url('admin/pages') }}">
                    <i class="fa fa-file-text"></i>
                    <span>{{__('layout.pages')}}</span>
                </a>
            </li>
            <li class="treeview">
                <a href="{{ url('admin/services/our-services') }}">
                    <i class="fa fa-scribd"></i>
                    <span>{{__('layout.services')}}</span>
                    <i class="fa fa-angle-double-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('admin/services/our-services') }}">{{__('layout.our_services')}}</a></li>
                    <li><a href="{{ url('admin/services/execution-order')  }}">{{__('layout.execution_order')}}</a></li>
                    <li><a href="{{ url('admin/services/bussiness-outline')  }}">{{__('layout.bussiness_outline')}}</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="{{ url('admin/galleries') }}">
                    <i class="fa fa-picture-o"></i>
                    <span>{{__('Gallery::gallery.galleries')}}</span>
                    <i class="fa fa-angle-double-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('admin/banners') }}">{{__('Gallery::gallery.banners')}}</a></li>
                    <li><a href="{{ url('admin/galleries') }}">{{__('Gallery::gallery.galleries')}}</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="{{ url('admin/users') }}">
                    <i class="fa fa-user"></i>
                    <span>{{__('layout.users')}}</span>
                    <i class="fa fa-angle-double-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('admin/users') }}">{{__('layout.users')}}</a></li>
                    <li><a href="{{ url('admin/roles') }}">{{__('layout.roles')}}</a></li>
                    <li><a href="{{ url('admin/groups') }}">{{__('layout.groups')}}</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="{{ url('admin/activity-logs') }}">
                    <i class="fa fa-history"></i>
                    <span>{{__('layout.activity_logs')}}</span>
                </a>
            </li>
            <li class="treeview">
                <a href="{{ url('admin/setting/general') }}">
                    <i class="fa fa-cog" aria-hidden="true"></i>
                    <span>Setting</span>
                    <i class="fa fa-angle-double-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('admin/setting/general') }}">General</a></li>
                    <li><a href="{{ url('admin/setting/cache') }}">Cache</a></li>
                </ul>
            </li>
        </ul>
    </section>
</aside>
