<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function getIndex(){
        $data = [
            'count_users' => \App\Models\User::count(),
            'count_posts' => \Modules\Post\Models\Post::count(),
            'count_contacts' => \App\Models\Contact::count()
        ];
        return view('admin.dashboard.index', compact('data'));
    }
}
