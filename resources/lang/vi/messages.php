<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Message Language Lines
    |--------------------------------------------------------------------------
    |   Coding : 'welcome' => 'Welcome to our application',
    |   Use it : {{ __('message.welcome')}}
    |   Display : Welcome to our application
    |
    */

    // Common Message
    'button_add_new' => 'Add new',
    'add_new'   => 'Add New',
    'cancel'    => 'Cancel',
    'category'  => 'Category',
    'create' => "Create",
    'created_at' => "Created Date",
    'date'      => 'Date',
    'end_date' => "End Date",
    'i_forgot_my_password' => 'I forgot my password',
    'month'     => 'Month',
    'welcome'   => 'Welcome to our application',
    'you_are_not_login' => "You're not login !",
    'remember_me' => 'Remember me',
    'start_date' => "Start Date",
    'search' => "Search",
    'updated_at' => "Updated Date",
    'sign_in' => 'Sign In',
    'sign_in_to_start_your_session' => 'Sign in to start your session',
    'update' => 'Update',
    'total' => 'Total',

    //Dashboard
    'dashboard'             => 'Dashboard',
    'effort_effeciency'     => 'Effort Effeciency',
    'cf_password'=>'Confirm Password',
    'success'=>'Success',
    'success_mail'=>'Yeu cau cua ban da duoc gui !',
    'send_pass'=>'We have sent an email with a link to reset your password.
                                            It may takes from 1 to 2 minutes for complete. Please check your inbox '
];
