$(document).ready(function () {
    $('#status').fadeToggle(3000);
    $('#img-preview[src=""]').hide();
    $('#img-preview:not([src=""])').show();

    $('#btn-logout').click(function (event) {
        event.preventDefault();
        swal({
            title: "Are you sure logout ?",
            text: "This action will end of your session.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((status) => {
                if (status) {
                    $.ajax({
                        type: 'POST',
                        url: $("#btn-logout").attr('href'),
                        cache: false,
                        success: function (data) {
                            location.reload();
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            iziToast.error({
                                title: "Error:",
                                message: thrownError,
                                position: 'topRight'
                            });
                        }
                    });
                }
            });


    });
});

function timeSince(date) {

    var seconds = Math.floor((new Date() - date) / 1000);

    var interval = Math.floor(seconds / 31536000);

    if (interval > 1) {
        return interval + " years";
    }
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) {
        return interval + " months";
    }
    interval = Math.floor(seconds / 86400);
    if (interval > 1) {
        return interval + " days";
    }
    interval = Math.floor(seconds / 3600);
    if (interval > 1) {
        return interval + " hours";
    }
    interval = Math.floor(seconds / 60);
    if (interval > 1) {
        return interval + " minutes";
    }
    return Math.floor(seconds) + " seconds";
}

$(".sidebar-menu li a").click(function () {
    $(this).parent().addClass('active').siblings().removeClass('active');
    $(".sidebar-menu .home").removeClass('active');

});

// CKfinder select file
function selectFileWithCKFinder(elementId) {
    CKFinder.modal({
        chooseFiles: true,
        width: 1000,
        height: 600,
        displayFoldersPanel: false,
        onInit: function (finder) {
            // choose file default.
            finder.on('files:choose', function (evt) {
                let file = evt.data.files.first();
                let fileUrl = file.getUrl();
                $(elementId).val(fileUrl.substring(fileUrl.lastIndexOf('/images') + 8));
                $('#img-preview').attr('src', fileUrl).show();
            });
            //if you want to resize and choose this custom file.
            finder.on('file:choose:resizedImage', function (evt) {
                let fileUrl = evt.data.resizedUrl;
                $(elementId).val(fileUrl.substring(fileUrl.lastIndexOf('/images') + 8));
                $('#img-preview').attr('src', fileUrl).show();
            });
        }
    });
}

// Review Image when upload file
function readImage(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#img-preview').attr('src', e.target.result).show();
        }
        reader.readAsDataURL(input.files[0]);
    }
}

//Convert to Slug
function convertToSlug(ele) {
    let text = ele.val().toLowerCase();
    text = text.replace(/[^\w- ]+/g, '').replace(/ +/g, '-');
    ele.val(text);
}

function readURL(input) {
    if (input.files && input.files[0]) {
        let reader = new FileReader();
        reader.onload = function (e) {
            $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload").change(function () {
    readURL(this);
});
$("#imageFavicon").change(function () {
    readURL1(this);
});
$(document).on("click", ".btn-clear-cache", function (e) {
    e.preventDefault();
    let t = $(e.currentTarget);
    t.addClass("button-loading"),
        $.ajax({
            url: t.data("url"),
            type: "POST",
            data: { type: t.data("type") },
            success: function (e) {
                console.log(e)
                t.removeClass("button-loading");
                iziToast.success({ position: "topRight", message: e.success_msg });
            },

        })
})
