<?php 

namespace Modules\Post\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Post\Repositories\Eloquents\{PostRepository, CategoryRepository};
use Modules\Post\Repositories\Contracts\{IPost, ICategory};

use File;

class MainServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/../../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'Post');
        $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');
        $this->loadTranslationsFrom(__DIR__ . '/../../resources/lang', 'Post');
    }

    public function register()
    {
        // Merge config
        $this->mergeConfigFrom(__DIR__ . '/../../config/post_config.php', 'post_config');

        // Add Provider for service container
        $this->app->bind(ICategory::class, CategoryRepository::class);
        $this->app->bind(IPost::class, PostRepository::class);

        // Load helper
        $this->autoload(__DIR__ . '/../../helpers');
    }

    public static function autoload($directory)
    {
        $helpers = File::glob($directory . '/*.php'); //get all files in directory
        // load file into App
        foreach ($helpers as $helper) {
            File::requireOnce($helper);
        }
    }
}