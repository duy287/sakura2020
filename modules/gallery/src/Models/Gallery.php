<?php

namespace Modules\Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{ 
    protected $fillable = ['image', 'size', 'sort_order'];
}
