<?php

namespace Modules\Post\Http\Requests;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Modules\Post\Models\Post;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('post');
        if($id){
            $rules = [
                'vi.title' => 'required',
                'slug' => 'required',
            ]; 
            $post = Post::find($id);
            if ($post->notHavingImageInDb()){
                $rules['featured_image'] = 'required';
            }
        }
        else{
            $rules = [
                'vi.title' => 'required',
                'slug' => 'required',
                'featured_image'=> 'required',
            ];    
        }
        return $rules;

    }

    public function attributes()
    {
        return [
            'vi.title' => trans('Post::post.title'),
            'slug' => trans('Post::post.slug'),
            'featured_image' => trans('Post::post.featured_image'),
        ];
    }

}
