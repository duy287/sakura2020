<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;
use App\Repositories\Contracts\IContact;
use App\Repositories\Contracts\IUser;
use App\Jobs\SendContactEmail;

use App\Notifications\NewContact;
use Notification;

class ContactController extends Controller
{
    protected $contactRepository, $userRepository;

    public function __construct(IContact $contactRepository, IUser $userRepository)
    {
        $this->contactRepository = $contactRepository;
        $this->userRepository = $userRepository;
    }

    public function sendContact(ContactRequest $request){
        $data = $request->except(['_token']);
        $contact = $this->contactRepository->create($data);

        //Send mail
        SendContactEmail::dispatch($contact)->delay(now()->addMinutes(2));

        //send notification
        $users = $this->userRepository->all();
        Notification::send($users, new NewContact($contact));

        return redirect()->route('frontend.home.index')->with([
            'flash_level' => 'success',
            'flash_message' => 'Send contact successfully'
        ]);
    }
}
