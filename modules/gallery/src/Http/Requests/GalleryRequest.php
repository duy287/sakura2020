<?php

namespace Modules\Gallery\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Modules\Gallery\Models\Banner;

class GalleryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
        return [
            'image'=> 'required|image|max:5000',
        ];   
    }

    public function attributes()
    {
        return [
            'image' => trans('Gallery::gallery.image')
        ];
    }
}
