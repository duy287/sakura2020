<?php
namespace App\Repositories\Eloquent;
use App\Models\Page;
use App\Repositories\Contracts\IPage;
use Illuminate\Support\Str;

class PageRepository extends BaseRepository implements IPage 
{
    public function model(){
        return Page::class;
    }

    public function datatable(){
        return datatables()->of($this->model::orderBy('created_at','desc')->get())
            ->editColumn('image', function ($item) {
                return image_thumb('pages/'.$item->image);
            })
            ->editColumn('type', function ($item) {
                $type = "Page";
                if($item->type==1)
                    $type = "About us";
                return "<span class='label-primary p-1'>$type</span>";
            })
            ->editColumn('content', function ($item) {
                return Str::limit($item->content, 60);
            })
            ->rawColumns(['image', 'type'])
            ->make(true);
    }
}
