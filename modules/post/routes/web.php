<?php

use Facade\FlareClient\View;
use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Modules\Post\Http\Controllers', 'middleware' => 'web'], function () {
    Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {

        Route::post('/categories/status/{id}', 'CategoryController@updateStatus')->name('admin.categories.status');
        Route::resource('/categories', 'CategoryController');

        Route::post('/posts/status/{id}', 'PostController@updateStatus')->name('admin.posts.status');
        Route::resource('/posts', 'PostController');
    });
});