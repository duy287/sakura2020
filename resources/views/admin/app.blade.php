<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin</title>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-176428314-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-176428314-1');
    </script>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Meta -->
    <meta name="locale" content="{{ App::getLocale() }}"/>

    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{asset('lib/bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{asset('lib/font-awesome/css/fontawesome.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('lib/adminlte/css/adminlte.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('lib/iCheck/square/blue.css')}}">
    <!-- Skin ALT Admin -->
    <link rel="stylesheet" href="{{asset('lib/skins/_all-skins.min.css')}}">
    <link rel="stylesheet" href="{{asset('lib/datatables/css/dataTables.bootstrap4.min.css')}}">
    <!-- IziToast -->
    <link rel="stylesheet" href="{{ asset('lib/iziToast/iziToast.min.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('lib/select2/css/select2.min.css') }}">
    <!-- Style css -->
    <link rel="stylesheet" href="{{asset('css/admin.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @stack('page-styles')

</head>
<body class="skin-blue sidebar-mini">
<div class="wrapper">
    <!-- Navbar -->
    @include('admin.navbar')

    <!-- Main Sidebar Container -->
    @include('admin.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div id="app">
        @yield('content')
    </div>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-light">
    </aside>
    <!-- /.control-sidebar -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
        </div>
        <strong>Copyright © 2018-<span id="year-now"></span> <a href="{{url('/')}}">SAKURA ECOLOGY  CO.,LTD</a>.</strong> All rights
        reserved.
    </footer>
</div>

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{asset('lib/jquery/jquery.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE -->
<script src="{{asset('lib/adminlte/js/adminlte.js')}}"></script>
<!-- ChartJS -->
{{-- <script src="{{asset('lib/chart.js/Chart.js')}}"></script> --}}
<!-- IziToast -->
<script src="{{ asset('lib/iziToast/iziToast.min.js') }}"></script>

<script src="{{ asset('js/app.js') }}"></script>
{{-- <script src="{{asset('lib/chart.js/Chart.min.js')}}"></script> --}}
<script src="{{asset('lib/datatables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lib/datatables/js/dataTables.bootstrap4.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/ckfinder/ckfinder.js') }}"></script>
<script type="text/javascript" src="{{ asset('lib/alert/sweetalert.min.js') }}"></script>
<script type="text/javascript">
    var now = new Date();
    $('#year-now').text(now.getFullYear());
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // Config ckfinder
    CKFinder.config({ 
        connectorPath: "{{ asset('ckfinder/connector') }}" ,
        language: "{{Session::get('lang')}}"
    });

    // Flash message
    @if (Session::has('flash_message'))
        let level = "{{ Session::get('flash_level') }}";
        let message = "{{ Session::get('flash_message') }}";
        switch (level) {
            case 'success':
                iziToast.success({
                    title: 'Message',
                    message: message,
                    position: 'topRight'
                });
                break;
            case 'warning':
                iziToast.warning({
                    title: 'Warning',
                    message: message,
                    position: 'topRight'
                });
                break;
            case 'error':
                iziToast.error({
                    title: 'Error',
                    message: message,
                    position: 'topRight',
                });
                break;
            default:
                break;
        }
    @endif

    var url = window.location;
    //Menu items
    var menuItem = $('ul.sidebar-menu a').filter(function () {
        return this.href == url;
    });
    menuItem.parentsUntil('ul.sidebar-menu', 'li').addClass('active');

    //Sub Menu items
    var subMenuItem = $('ul.treeview-menu a').filter(function () {
        return this.href == url;
    });
    subMenuItem.parent().addClass('active');
</script>
<script src="{{ asset('js/admin.js') }}"></script>
@stack('page-scripts')
</body>
</html>
