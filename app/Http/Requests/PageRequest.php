<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Page;

class PageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $page_id= $this->route('page');
        if($page_id){
            $rules = [
                'vi.title' => 'required',
                'type' => 'required',
            ]; 
            $page = Page::find($page_id);
            if ($page->notHavingImageInDb()){
                $rules['image'] = 'required';
            }
        }
        else{
            $rules =  [
                'vi.title' => 'required',
                'type' => 'required',
                'image'=> 'required',
            ];    
        }
        return $rules;
    }

    public function attributes(){
        return [
            'vi.title'=> trans('layout.title')
        ];
    }
}
