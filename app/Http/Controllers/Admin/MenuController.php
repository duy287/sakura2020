<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Contracts\IMenu;
use App\Http\Requests\MenuRequest;
use Illuminate\Support\Str;

class MenuController extends Controller
{
    protected $menuRepository;

    public function __construct(IMenu $menuRepository)
    {
        $this->menuRepository = $menuRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->menuRepository->datatable();
        }
        return View('admin.menu.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.menu.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MenuRequest $request)
    {
        $data = $request->except(['_token']);
        $data['slug'] = Str::slug($data['slug']);
        $this->menuRepository->create($data);

        return redirect()->route('menus.index')->with([
            'flash_level'   => 'success',
            'flash_message' =>  trans('action.add_success', ['m'=>'menu'])
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $menu = $this->menuRepository->find($id);
        return view('admin.menu.edit', compact('menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MenuRequest $request, $id)
    {
        $data = $request->except(['_token']);
        $data['slug'] = Str::slug($data['slug']);
        $this->menuRepository->update($id, $data);

        return redirect()->route('menus.index')->with([
            'flash_level'   => 'success',
            'flash_message' => trans('action.update_success', ['m'=>'menu'])
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($this->menuRepository->delete($id)){
            return response()->json([
                'row_id' => $id,
                'message' => trans('action.delete_success', ['m'=>'Menu']),
                'status_code' => 200
            ], 200);
        }
        else{
            return response()->json([
                'row_id' => $id,
                'message' => trans('action.has_error'),
                'status_code' => 500
            ], 500);
        }
    }
}
