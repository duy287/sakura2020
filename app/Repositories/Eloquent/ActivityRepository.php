<?php
namespace App\Repositories\Eloquent;
use App\Repositories\Contracts\IActivity;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class ActivityRepository implements IActivity
{
    public function datatable($request){
        $query = DB::table('activity_log')
        ->select('users.name', 'activity_log.causer_id', 'activity_log.properties', 'activity_log.description', 'activity_log.created_at')
        ->join('users', 'activity_log.causer_id', '=', 'users.id');
        //search
        if($request->from){
            $from_date = Carbon::parse($request->from);
            $query->whereDate('activity_log.created_at', '>=' , $from_date);
        }
        if($request->to){
            $to_date = Carbon::parse($request->to);
            $query->whereDate('activity_log.created_at', '<=', $to_date);
        }
        // order
        $query->orderBy('created_at', 'DESC')->get();
        return datatables($query)->make(true);
    }

    public function clear($request){
        $query = DB::table('activity_log');
        if($request->from){
            $from_date = Carbon::parse($request->from);
            $query->whereDate('activity_log.created_at', '>=' , $from_date);
        }
        if($request->to){
            $to_date = Carbon::parse($request->to);
            $query->whereDate('activity_log.created_at', '<=', $to_date);
        }
        $query->delete();
        return true;
    }
}
