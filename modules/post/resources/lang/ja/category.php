<?php
return [
    'categories' => 'カテゴリー',
    'category_list' => 'カテゴリー一覧',
    'add_category' => '新しいカテゴリを追加',
    'edit_category' => 'カテゴリを編集',
    'slug'  => 'リンク',
    'status' => '状態',
    'active' => 'アクティブ',
    'parent' => '父親カタログ',
    'name' => '名前',
    'permalink' => 'パーマリンク'
];