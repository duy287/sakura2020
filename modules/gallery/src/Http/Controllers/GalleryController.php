<?php

namespace Modules\Gallery\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Gallery\Repositories\Contracts\IGallery;
use Modules\Gallery\Http\Requests\GalleryRequest;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class GalleryController extends Controller
{
    protected $galleryRepository;

    public function __construct(IGallery $galleryRepository)
    {
        $this->galleryRepository = $galleryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galleries = $this->galleryRepository->all();
        return view('Gallery::gallery.index', compact('galleries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GalleryRequest $request)
    {
        $filename = '';
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $filename = time().'_'.$file->getClientOriginalName();
            Storage::putFileAs('images/galleries/', $file, $filename);
        }

        $this->galleryRepository->create([
           'image'=> $filename,
           'size'=> Storage::size('images/galleries/'.$filename),
        ]);
        
        return redirect()->route('galleries.index')->with([
            'flash_level'   => 'success',
            'flash_message' => trans('action.add_success', ['m'=>'gallery'])
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gallery = $this->galleryRepository->find($id);
        //remove old file
        if($gallery->image){
            try {
                Storage::delete('images/galleries/' . $gallery->image);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }
        
        if($this->galleryRepository->delete($id)){
            return response()->json([
                'row_id' => $id,
                'message' => trans('action.delete_success', ['m'=>'gallery']),
                'status_code' => 200
            ], 200);
        }
        else{
            return response()->json([
                'row_id' => $id,
                'message' => trans('action.has_error'),
                'status_code' => 500
            ], 500);
        }
    }
}
