@extends('admin.mail_layout')

@section('mail-content')
    <table border="0" bgcolor="#ffffff" class="wrapper-mail" width="500" cellpadding="0" cellspacing="0"
           style="padding:10px; line-height:100%; margin: auto">
        <thead>
        <tr>
            <td style="padding-top: 20px">
                <big>Hello Minh Hao. </big>
            </td>
        </tr>
        </thead>
        <tbody  style="width: 100%; margin-bottom: 15px">
        <tr>
            <td style="padding: 15px 0 60px 0; ;position: relative">
                <p>Mail này được gửi để test khả năng gửi của email bạn.Email này hợp l
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <table border="0" align="center" width="240" cellpadding="0" cellspacing="0"
                       bgcolor="5caad2" style="margin-bottom:20px;">

                    <tr>
                        <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                    </tr>

                    <tr>
                        <td align="center"
                            style="color: #ffffff; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 22px; letter-spacing: 2px;">
                            <!-- main section button -->

                            <div style="line-height: 22px; padding: 0 20px">
                                <a href="{{url('/report')}}"
                                   style="color: #ffffff; text-decoration: none;">ウェブサイト</a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                    </tr>

                </table>
            </td>
        </tr>
        {{--    <tr><td align="center"><button class="link">    Click To Report</button></td></tr>--}}
        </tbody>
        <tfoot style="margin-top: 10px">
        <tr border="1">
            <td style="margin-top:5px; padding: 20px 0 40px 0; border-top: 2px solid #7e57c2 ;color: #7e57c2">
                <span style="font-size: 20px">  Thank you</span>,
                <br>
                <span style="font-weight: 600 ; padding-top: 5px">
                          The CALC Team
                    </span>
            </td>
        </tr>
        </tfoot>

    </table>
@endsection
