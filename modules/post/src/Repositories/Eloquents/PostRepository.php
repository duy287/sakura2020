<?php
namespace Modules\Post\Repositories\Eloquents;
use Modules\Post\Repositories\Contracts\IPost;
use App\Repositories\Eloquent\BaseRepository;
use Modules\Post\Models\Post;

class PostRepository extends BaseRepository implements IPost 
{
    public function model(){
        return Post::class ;
    }

    public function datatable(){
        return datatables()->of($this->model::with('category')->orderBy('created_at','desc')->get())
        ->addColumn('category', function ($item){
            return $item->category->name;
        })
        ->editColumn('featured_image', function ($item) {
            return image_thumb('posts/'.$item->featured_image);
        })
        ->editColumn('status', function ($item) {
            return status_checkbox($item);
        })
        ->rawColumns(['category', 'status'])
        ->make(true);
    }
}
