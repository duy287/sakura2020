<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Page extends Model
{
    use Translatable;
    public $translatedAttributes = ['title', 'content'];
    protected $fillable = ['image', 'type', 'status'];

    public function notHavingImageInDb(){
        return (empty($this->image))?true:false;
    }
}
