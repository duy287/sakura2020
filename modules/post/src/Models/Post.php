<?php

namespace Modules\Post\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Post extends Model implements TranslatableContract
{ 
    use Translatable;
    public $translatedAttributes = ['title', 'content'];
    protected $fillable = ['type', 'author', 'slug', 'status', 'tags', 'featured_image', 'category_id'];

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function notHavingImageInDb(){
        return (empty($this->featured_image)) ? true:false;
    }
}
