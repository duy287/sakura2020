<?php
namespace Modules\Post\Repositories\Eloquents;
use Modules\Post\Repositories\Contracts\ICategory;
use App\Repositories\Eloquent\BaseRepository;
use Modules\Post\Models\Category;

class CategoryRepository extends BaseRepository implements ICategory 
{
    public function model(){
        return Category::class;
    }

    public function datatable(){
        return datatables()->of($this->model::orderBy('created_at','desc')->get())
        ->addColumn('parent', function ($item){
            if($item->parent_id){
                return $this->model->find($item->parent_id)->name;
            }
            return null;
        })
        ->editColumn('status', function ($item) {
            return status_checkbox($item);
        })
        ->rawColumns(['image', 'type_name', 'status'])
        ->make(true);
    }
}
