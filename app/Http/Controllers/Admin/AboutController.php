<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Supports\SettingStore;
use App\Models\Setting;

class AboutController extends Controller
{
    protected $settingStore;

    public function __construct(SettingStore $settingStore)
    {
        $this->settingStore = $settingStore;
    }
    /**
     * Display setting list.
     *
     * @return Illuminate\Support\Facades\View;
     */
    public function index()
    {
        $options = Setting::pluck('key', 'value')->all();
        return View('admin.about.index', compact('options'));
    }

    /**
     * Store data setting.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Illuminate\Support\Facades\View;
     */
    public function store(Request $request)
    {
        $data = $request->except(['_token']);
        foreach($data as $key=>$value){
            $this->settingStore->set($key, $value);
        }
        $this->settingStore->save();

        return redirect()->route('about.index')->with([
            'flash_level'   => 'success',
            'flash_message' =>  trans('action.update_success', ['m' => 'Setting'])
        ]);
    }
}
