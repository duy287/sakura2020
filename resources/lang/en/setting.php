<?php
return [
    'setting_mail'            => 'Setting for email template',
    'setting_des'            => 'Email template using HTML & system variables.',
    'driver'            => 'Driver',
    'port'        => 'Port',
    'name'             => 'Name',
    'host'        => 'Host',
    'us'      => 'Username',
    'pass'      => 'Password',
    'encryption'      => 'Encryption',
    'sender_name'         => 'Sender name',
    'sender_mail'      => 'Sender email',
    'send'=> 'Send test a mail',
    'save'        => 'Save Setting',
    'send_des'  => 'To send test email, please make sure you are updated configuration to send mail!',
    'notification'       => 'Save Setting Success.',
    'err'=>'Send mail test Error. Please check your email settings ',
    'general'        => 'General Setting',
    'logo'        => 'Admin Logo',
    'favicon'        => 'Admin Favicon',
    'choose'        => 'Choose File',
    'title'        => 'Admin Title',
    'change'        => 'Change',
    'exchange-rate'=> 'Exchange rate',
    'cache_cm'=>' Clear cache commands',
    'cache_c'        => 'Clear CMS caching: database caching, static blocks... Run this command when you don\'t see the changes after updating data.',
    'view_c'        => 'Clear compiled views to make views up to date.',
    'config_c'        => 'You might need to refresh the config caching when you change something on production environment.',
    'route_c'        => 'Clear cache routing.',
    'sys_c'        => 'Clear system log files',
    'cache_b'        => 'Clear all system cache',
    'view_b'        => 'Refresh compiled views',
    'config_b'        => 'Clear config route',
    'route_b'        => 'Clear route cache',
    'sys_b'        => 'Clear log',
    'cache_msg'        => 'Clear cache success',
    'mail_msg'        => 'Send mail success,',
    'mail_err'=>'There was an error, please check your mail information ',

    'about_us'=>'About us',
    'about_info'=>'Set up contact information',
    'company_name'=>'Company name',
    'head_office_address'=> 'Head office address',
    'branch_address'=>'Branch address',
    'tel'=>'Tel',
    'fax'=>'Fax',
    'hotline'=>'Hotline',
    'contact_email'=>'Contact email',
    'about'=>'About'

];
