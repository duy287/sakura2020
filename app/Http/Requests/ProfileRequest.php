<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Validator;
use Illuminate\Support\Facades\Auth;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user_id = Auth::id();
        return [
            'name' =>'required',
            'username'=>"required|min:4|unique:users,username,".$user_id,
            'email' => "required|email|unique:users,email,".$user_id,
            'avatar'=> 'image',
        ];
    }
}
