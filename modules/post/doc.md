# Hướng dẫn Posts Module 

## Cài đặt
1. Đăng ký package với service provider: config/app.php
'providers' => [
    ...
    Modules\Posts\Providers\MainServiceProvider::class
]
2. Đăng ký Namespace trong composer.json
"autoload": {
    "psr-4": {
        "App\\": "app/",
        "Modules\\Posts\\": "modules/posts/src" 
    },
    ...`
}
3. decleration webpack
`mix.js('modules/posts/resources/js/posts.js', 'public/js')`
- Import JS vào views: `<script src="{{asset('js/posts.js')}}"></script>`

4. Run migration
`php artisan migrate`

## Sử dụng
1. Controller
Namespace: `Modules\Posts\Http\Controllers;`
2. View
`view('Posts::welcome')`
3. Models
`use Modules\Posts\Models\Post;`
4. Config
`config('post_ml.post')`
5. Repository
`use Modules\Posts\Repositories\Interfaces\PostInterface;`
6. Migration
- Create migration
`php artisan make:migration create_posts_table --path=modules/posts/database/migrations`
- Run migration
`php artisan migrate `
7. Translation
`{{__('Posts::action.create')}}`

## Package 
### Preloader
https://gasparesganga.com/labs/jquery-loading-overlay/#methods

### Store 
php artisan storage:link

### Mailer

