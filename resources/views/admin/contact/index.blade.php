@extends('admin.app')

@section('title', 'SKR | User')
@section('content')
<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <span class="lead"><i class="fa fa-list-ul" aria-hidden="true"></i> {{__('layout.contact_list')}}</span>
                    </div>
                    <div class="card-body ">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{__('layout.name')}}</th>
                                        <th>{{__('layout.email')}}</th>
                                        <th>{{__('layout.message')}}</th>
                                        <th>{{__('layout.status')}}</th>
                                        <th>{{__('layout.action')}}</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="confirmDeleteItem" tabindex="-1" role="dialog" aria-labelledby="modelLabel"
          aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    {{__('action.delete_confirm')}}
                </div>
                <div id="confirmMessage" class="modal-body">
                    {{__('action.delete_sure', ['obj'=>'role'])}}
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnConfirmDelete" class="btn btn-danger btn-ok"
                            onclick="deleteItem($(this).val())">
                        {{__('action.delete')}}
                    </button>
                    <button type="button" id="confirmCancel" class="btn btn-secondary" data-dismiss="modal"
                            data-toggle="modal" data-target="#confirmDeleteUser">
                        {{__('action.cancel')}}
                    </button>
                </div>
            </div>
        </div>
    </div>
  </section>
</div>
@endsection

@push('page-scripts')
<script>
    $('#dataTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('contacts.index') }}",
        columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex' },
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            { data: 'message', name: 'message' },
            { data: 'status', name: 'status' },
        ],
        columnDefs: [{
            "targets": 5, 
            "data": null,
            "render": function (data) {
                detail_url = "{{ url('admin/contacts') }}/" + data.id;
                delete_event = ' onclick = checkItemExisted(' + data.id + ') ';
                return '<a class="btn btn-primary btn-sm" href=' + detail_url + '><i class="fa fa-eye" aria-hidden="true"></i></a> ' +
                '<button class="btn btn-sm btn-danger" ' + delete_event + 'id="row_' + data.id + '" data-toggle="modal" data-target="#confirmDeleteItem" >' +
                    '<span class="fa fa-trash" ></span></button> ';
            }
        }],
    });

    function checkItemExisted(id) {
        $('#btnConfirmDelete').val(id);
    }

    function deleteItem(id) {
        let delete_URL = "{{ url('admin/contacts') }}/" + id;
        let request = $.ajax({
            url: delete_URL,
            method: "DELETE"
        }).done(function (data) {
            $("#confirmDeleteItem").modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            if(data.status_code == 500){
                iziToast.success({
                    title: 'Error',
                    message: data.message,
                    position: 'topRight'
                });
            }
            else {
                $(`#row_${data.row_id}`).closest("tr").remove();
                iziToast.success({
                    title: 'Message',
                    message: data.message,
                    position: 'topRight'
                });
            }
        });
    }
</script>
@endpush

