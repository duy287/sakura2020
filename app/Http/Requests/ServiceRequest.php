<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Service;

class ServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id= $this->route('service');
        if($id){
            $rules = [
                'vi.title' => 'required',
                'vi.description' => 'required',
                'type'=>'required'
            ]; 
            $service = Service::find($id);
            if ($service->notHavingImageInDb()){
                $rules['image'] = 'required|image';
            }
        }
        else{
            $rules =  [
                'vi.title' => 'required',
                'vi.description' => 'required',
                'image'=> 'required|image',
                'type'=>'required'
            ];    
        }
        return $rules;
    }

    public function attributes(){
        return [
            'vi.title'=> trans('layout.title'),
            'vi.description'=> trans('layout.description'),
            'type'=> trans('layout.type')
        ];
    }
}
