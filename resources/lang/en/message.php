<?php

// resources/lang/en/messages.php

return [
    /*
    |--------------------------------------------------------------------------
    | Message Language Lines
    |--------------------------------------------------------------------------
    |   Coding : 'welcome' => 'Welcome to our application',
    |   Use it : {{ __('message.welcome')}}
    |   Display : Welcome to our application
    |
    */

    // Common Message
    'received_new_contact' => 'You have just received :value new contacts',
    'no_new_contact' => 'No new contacts',
    'incorrect_password' => 'Incorrect passowrd',
    'change_passpword_success' => 'Change password successfully',
];
