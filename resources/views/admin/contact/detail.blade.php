@extends('admin.app')

@section('title', 'SKR | Users')

@section('content')
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-9">
                    <div class="card card-primary card-outline">
                        <div class="card-header"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{__('layout.contact_detail')}}</div>
                        <div class="card-body">
                            <div class="jumbotron">
                                <p><b>{{__('layout.name')}}:</b> {{ $contact->name }}</p>
                                <p><b>{{__('layout.email')}}:</b> {{ $contact->email }}</p>
                                <p><b>{{__('layout.date_time')}}:</b> {{ $contact->created_at }}</p>
                                <p><b>{{__('layout.status')}}:</b> {{ $contact->status==1?'Success' : 'pending'}}</p>
                                <hr class="my-4">
                                <p class="lead">{{__('layout.message')}}:</p>
                                <p>{{ $contact->message}}</p>
                            </div>
                            <div class="text-right">
                                <a href="{{route('contacts.index')}}" class="btn btn-primary btn-sm">{{__('layout.back_to_contact_list')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
