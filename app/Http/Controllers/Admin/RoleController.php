<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Contracts\IRole;
use App\Http\Requests\RoleRequest;

class RoleController extends Controller
{
    protected $roleRepository;

    public function __construct(IRole $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->roleRepository->datatable();
        }
        return View('admin.role.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $permissions = config('permission');
        return view('admin.role.add', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        $data = $request->except(['_token']);
        $data['permission'] = json_encode($data['permission']);
        $data['created_by'] = $request->user()->name;

        $this->roleRepository->create($data);
        return redirect()->route('roles.index')->with([
            'flash_level'   => 'success',
            'flash_message' => trans('action.add_success', ['obj'=>'role'])
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $role = $this->roleRepository->find($id);
        $permissions = config('permission');
        $role_permission =  (array) json_decode($role->permission);
        return view('admin.role.edit', compact('role', 'permissions', 'role_permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request, $id)
    {
        $data = $request->except(['_token']);
        $data['permission'] = json_encode($data['permission']);
        $this->roleRepository->update($id, $data);
        
        return redirect()->route('roles.index')->with([
            'flash_level'   => 'success',
            'flash_message' => trans('action.update_success', ['obj'=>'role'])
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($this->roleRepository->delete($id)){
            return response()->json([
                'row_id' => $id,
                'message' => trans('action.delete_success', ['obj'=>'role']),
                'status_code' => 200
            ], 200);
        }
        else{
            return response()->json([
                'row_id' => $id,
                'message' => trans('has_error'),
                'status_code' => 500
            ], 500);
        }
    }
}
