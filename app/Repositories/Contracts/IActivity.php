<?php
namespace App\Repositories\Contracts;

interface IActivity{
    public function datatable($request);
    public function clear($request);
}
