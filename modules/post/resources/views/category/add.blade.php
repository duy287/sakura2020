@extends('admin.app')

@section('title', 'SKR | Users')

@section('content')
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-8">
                    <div class="card card-primary card-outline">
                        <div class="card-header"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{__('Post::category.add_category')}}</div>
                        <div class="card-body">
                            <form action="{{route('categories.store')}}" method="POST">
                                @csrf
                                <div class="form-group">  
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="vi-tab" data-toggle="tab" href="#vi" role="tab" aria-controls="vi" aria-selected="false">{{__('layout.vietnamese')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="ja-tab" data-toggle="tab" href="#ja" role="tab" aria-controls="ja" aria-selected="true">{{__('layout.japanese')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en" aria-selected="false">{{__('layout.english')}}</a>
                                        </li>
                                    </ul>
                                
                                    <div class="tab-content tabs-trans" id="myTabContent">
                                        <div class="tab-pane fade show active" id="vi" role="tabpanel" aria-labelledby="vi-tab">
                                            <div class="form-group">
                                                <label for="name">{{__('Post::category.name')}}</label>
                                                <input type="text" id="name" name="vi[name]" value="{{ old('vi.name') }}" class="form-control
                                                @error('vi.name') is-invalid @enderror">
                                                @error('vi.name')<div class="invalid-feedback">{{ $message }}</div>@enderror
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="ja" role="tabpanel" aria-labelledby="ja-tab">
                                            <div class="form-group">
                                                <label for="name">{{__('Post::category.name')}}</label>
                                                <input type="text" id="name" name="ja[name]" value="{{ old('ja.name') }}" 
                                                    class="form-control">
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
                                            <div class="form-group">
                                                <label for="name">{{__('Post::category.name')}}</label>
                                                <input type="text" id="name" name="en[name]" value="{{ old('en.name') }}" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="slug">{{__('Post::category.permalink')}}</label>
                                    <div class="d-flex text-black">
                                        <span class="default-slug"><mark>{{url('/')}}/<mark></span>
                                        <div class="input-slug">
                                            <input type="text" id="slug" name="slug" value="{{ old('slug') }}" class="form-control @error('slug') is-invalid @enderror" 
                                                onfocusout="convertToSlug($(this))">
                                            @error('slug')<div class="invalid-feedback">{{ $message }}</div>@enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputState">{{__('Post::category.parent')}}</label>
                                    <select id="inputState" class="form-control" name="parent_id">
                                        <option value="" selected>None</option>
                                        @foreach ($categories as $cate)
                                            <option value="{{$cate->id}}">{{ $cate->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                {{-- <div class="form-group">
                                    <div class="form-check">
                                      <input class="form-check-input" type="checkbox" id="gridCheck" name="status" value="1">
                                      <label class="form-check-label" for="gridCheck">
                                        Active
                                      </label>
                                    </div>
                                </div> --}}
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i> Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
