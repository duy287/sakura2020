<?php

namespace Modules\Gallery\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Banner extends Model implements TranslatableContract
{ 
    use Translatable;
    public $translatedAttributes = ['title', 'description'];
    protected $fillable = ['link', 'image', 'status', 'type', 'sort_order'];

    public function notHavingImageInDb()
    {
        return (empty($this->image))?true:false;
    }
}
