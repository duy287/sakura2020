<?php
    /*
    |--------------------------------------------------------------------------
    | Permission user
    |--------------------------------------------------------------------------
    |
    */
    return [
        'contact' => [
            'view_contacts',
            'delete_contact'
        ],
        'user' => [
            'view_users',
            'create_user',
            'edit_user',
            'delete_user'
        ],
        'role' => [
            'view_roles',
            'create_role',
            'edit_role',
            'delete_role'
        ],
        'group' => [
            'view_groups',
            'create_group',
            'edit_group',
            'delete_group'
        ],
        'menu' => [
            'view_menus',
            'create_menu',
            'edit_menu',
            'delete_menu'
        ],
        'category'=>[
            'view_categories',
            'create_category',
            'edit_category',
            'delete_category'
        ],
        'post'=>[
            'view_posts',
            'create_post',
            'edit_post',
            'delete_post'
        ],
        'gallery' => [
            'view_galleries',
            'create_gallery',
            'edit_gallery',
            'delete_gallery'
        ],
        'page' =>[
            'view_pages',
            'create_page',
            'edit_page',
            'delete_page'
        ]
    ];