@extends('admin.app')

@section('title', 'SKR | Users')

@section('content')
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
        <form action="{{route('posts.update', $post->id)}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-md-8">
                    <div class="card card-primary card-outline">
                        <div class="card-header"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{__('Post::post.edit_post')}}</div>
                        <div class="card-body">
                            <div class="form-group">  
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="vi-tab" data-toggle="tab" href="#vi" role="tab" aria-controls="vi" aria-selected="false">{{__('layout.vietnamese')}}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="ja-tab" data-toggle="tab" href="#ja" role="tab" aria-controls="ja" aria-selected="true">{{__('layout.japanese')}}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en" aria-selected="false">{{__('layout.english')}}</a>
                                    </li>                                 
                                </ul>
                            
                                <div class="tab-content tabs-trans" id="myTabContent">
                                    <!--Vietnamese-->
                                    <div class="tab-pane fade show active" id="vi" role="tabpanel" aria-labelledby="vi-tab">
                                        <div class="form-group">
                                            <label>{{__('Post::post.title')}}</label>
                                            <input type="text" name="vi[title]" value="{{ old('vi.title', $post->{'title:vi'}) }}" class="form-control
                                            @error('vi.title') is-invalid @enderror">
                                            @error('vi.title')<div class="invalid-feedback">{{ $message }}</div>@enderror
                                        </div>
                                        <div class="form-group">
                                            <label>{{__('Post::post.content')}}</label>
                                            <textarea class="form-control" rows="3" name="vi[content]">{{ old('vi.content', $post->{'content:vi'}) }}</textarea>
                                        </div>
                                    </div>
                                    <!--Japanese-->
                                    <div class="tab-pane fade" id="ja" role="tabpanel" aria-labelledby="ja-tab">
                                        <div class="form-group">
                                            <label>{{__('Post::post.title')}}</label>
                                            <input type="text" name="ja[title]" value="{{ old('ja.title', $post->{'title:ja'}) }}" 
                                                class="form-control">                                            
                                        </div>
                                        <div class="form-group">
                                            <label>{{__('Post::post.content')}}</label>
                                            <textarea class="form-control" rows="3" name="ja[content]">{{ old('ja.content', $post->{'content:ja'}) }}</textarea>
                                        </div>
                                    </div>
                                    <!--English-->
                                    <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
                                        <div class="form-group">
                                            <label>{{__('Post::post.title')}}</label>
                                            <input type="text" name="en[title]" value="{{ old('en.title', $post->{'title:en'}) }}" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>{{__('Post::post.content')}}</label>
                                            <textarea class="form-control" rows="3" name="en[content]">{{ old('en.content', $post->{'content:en'}) }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="slug">{{__('Post::post.slug')}}</label>
                                <input type="text" id="slug" name="slug" value="{{ old('slug', $post->slug) }}" 
                                    class="form-control @error('slug') is-invalid @enderror" 
                                    onfocusout="convertToSlug($(this))">
                                @error('slug')<div class="invalid-feedback">{{ $message }}</div>@enderror
                            </div>
                            <div class="form-group">
                                <label for="categories">{{__('Post::post.belong_to_category')}}</label>
                                <select id="categories" class="form-control" name="category_id">
                                    @foreach ($categories as $cate)
                                        <option value="{{$cate->id}}" {{$cate->id==$post->category_id? 'selected': ''}}>{{ $cate->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" {{ $post->status==1? 'checked': ''}} type="checkbox" id="gridCheck" name="status" value="1">
                                    <label class="form-check-label" for="gridCheck">Active</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-right">
                    <div class="card card-primary card-outline">
                        <div class="card-body">
                            <label for="">{{__('Post::post.featured_image')}}</label>
                            <div class="form-group">
                                {{-- <button id="ckfinder-modal" class="btn btn-primary btn-sm">{{__('action.browse_server')}}</button>
                                <input id="ckfinder-input" name="featured_image" hidden> --}}

                                <label class="upload-photo btn btn-success btn-sm" for="upload-photo">{{__('action.upload_from_client')}}</label>
                                <input type="file" onchange="readImage(this)" name="featured_image" id="upload-photo" hidden/>

                                <img id="img-preview" src="{{ asset("/storage/images/posts/$post->featured_image") }}" class="img-thumbnail">
                                @error('featured_image')<div class="text-danger">{{ $message }}</div>@enderror
                            </div>
                            <hr>
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i> Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        </section>
    </div>
@endsection
@push('page-scripts')
<script type="text/javascript" src="{{ asset('lib/ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.replace('ja[content]', {
        filebrowserBrowseUrl: "{{ route('ckfinder_browser') }}",
        filebrowserUploadUrl: "{{ route('ckfinder_connector') }}?command=QuickUpload&type=Files",
    });
    CKEDITOR.replace('en[content]', {
        filebrowserBrowseUrl: "{{ route('ckfinder_browser') }}",
        filebrowserUploadUrl: "{{ route('ckfinder_connector') }}?command=QuickUpload&type=Files",
    });
    CKEDITOR.replace('vi[content]', {
        filebrowserBrowseUrl: "{{ route('ckfinder_browser') }}",
        filebrowserUploadUrl: "{{ route('ckfinder_connector') }}?command=QuickUpload&type=Files",
    });

    //button show model
    var button = $('#ckfinder-modal');
    button.click(function(e){
        e.preventDefault();
        selectFileWithCKFinder('#ckfinder-input');
    });
    </script>
@endpush
