@extends('admin.app')

@section('title', 'SKR | Users')

@section('content')
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-9">
                    <div class="card card-primary card-outline">
                        <div class="card-header"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{__('layout.edit_role')}}</div>
                        <div class="card-body">
                            <form action="{{route('roles.update', $role->id)}}" method="POST">
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <label for="name">{{__('layout.name')}}</label>
                                    <input type="text" id="name" name="name" value="{{ old('name', $role->name) }}" 
                                        class="form-control @error('name') is-invalid @enderror">
                                    @error('name')<div class="invalid-feedback">{{ $message }}</div>@enderror
                                </div>
                                <div class="form-group">
                                    <label for="description">{{__('layout.description')}}</label>
                                    <textarea name="description" placeholder="Short description" 
                                    class="form-control">{{ old('description', $role->description) }}</textarea>
                                </div>
                                <div class="widget meta-boxes">
                                    <div class="widget-title">
                                        <h4>{{__('layout.per_flags')}}</h4>
                                    </div>
                                    <div class="widget-body">
                                        <ul class="tree">
                                            @foreach ($permissions as $per_key => $permission)
                                            <li class="has">
                                                <input type="checkbox" class="check-group">
                                                <label>{{__("permission.$per_key")}} <span class="total">({{ count($permission) }})</span></label>
                                                <ul class="tree-child">
                                                    @foreach ($permission as $value)
                                                    <li>
                                                        <input type="checkbox" name="permission[]" value="{{ $value }}" {{ in_array($value, $role_permission) ? 'checked' : '' }}>
                                                        <label>{{__("permission.$value")}}</label>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i> {{__('action.save')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('page-scripts')
<script>
//https://codepen.io/parthviroja/pen/jPdZoM
$(document).on('click', '.tree label', function(e) {
    $(this).next('ul').toggle();
    e.stopPropagation();
});

$(document).on('change', '.tree input[type=checkbox]', function(e) {
    $(this).siblings('ul').find("input[type='checkbox']").prop('checked', this.checked);
    $(this).parentsUntil('.tree').children("input[type='checkbox']").prop('checked', this.checked);
    e.stopPropagation();
});

$("input.check-group").each(function () {
    let role = $(this);
    let childs = role.siblings('.tree-child').find("input[type='checkbox']:checked");
    if(childs.length > 0){
        role.prop('checked', true);
    }
});
</script>
@endpush
