@extends('admin.app')

@section('title', 'SKR | User')
@section('content')
<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <span class="lead"><i class="fa fa-list-ul" aria-hidden="true"></i> {{__('Post::post.post_list')}}</span>
                        <a href="{{ route('posts.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i>
                            {{__('action.create')}}
                        </a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>{{__('Post::post.title')}}</th>
                                        <th>{{__('Post::post.featured_image')}}</th>
                                        <th>{{__('Post::post.belong_to_category')}}</th>
                                        <th>{{__('Post::post.active')}}</th>
                                        <th>{{__('layout.action')}}</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="confirmDeleteItem" tabindex="-1" role="dialog" aria-labelledby="modelLabel"
          aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    {{__('action.delete_confirm')}}
                </div>
                <div id="confirmMessage" class="modal-body">
                    {{__('action.delete_sure', ['obj'=>'Post'])}}
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnConfirmDelete" class="btn btn-danger btn-ok"
                            onclick="deleteItem($(this).val())">
                        {{__('action.delete')}}
                    </button>
                    <button type="button" id="confirmCancel" class="btn btn-secondary" data-dismiss="modal"
                            data-toggle="modal" data-target="#confirmDeleteUser">
                        {{__('action.cancel')}}
                    </button>
                </div>
            </div>
        </div>
    </div>
  </section>
</div>
@endsection

@push('page-scripts')
<script>
    $('#dataTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('posts.index') }}",
        columns: [
            { data: 'title', name: 'title' },
            { data: 'featured_image', name: 'featured_image' },
            { data: 'category', name: 'category' },
            { data: 'status', name: 'status' }
            
        ],
        columnDefs: [{
            "targets": 4, 
            "data": null,
            "render": function (data) {
                edit_url = "{{ url('admin/posts') }}/" + data.id + "/edit";
                delete_event = ' onclick = checkItemExisted(' + data.id + ') ';
                return '<a class="btn btn-primary btn-sm" href=' + edit_url + '><span class="fa fa-edit"></span></a> ' +
                '<button class="btn btn-sm btn-danger" ' + delete_event + 'id="row_' + data.id + '" data-toggle="modal" data-target="#confirmDeleteItem" >' +
                '<span class="fa fa-trash" ></span></button> ';
            }
        }],
    });

    function checkItemExisted(id) {
        $('#btnConfirmDelete').val(id);
    }

    function deleteItem(id) {
        let delete_URL = "{{ url('admin/posts') }}/" + id;
        let request = $.ajax({
            url: delete_URL,
            method: "DELETE"
        }).done(function (data) {
            $("#confirmDeleteItem").modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            
            if(data.status_code == 500){
                iziToast.success({
                    title: 'Error',
                    message: data.message,
                    position: 'topRight'
                });
            }
            else {
                $(`#row_${data.row_id}`).closest("tr").remove();
                iziToast.success({
                    title: 'Message',
                    message: data.message,
                    position: 'topRight'
                });
            }
        });
    }

    function changeStatus(id){
        ajax_URL = "{{ url('admin/posts/status') }}/" + id;
        let request = $.ajax({
            url: ajax_URL,
            method: "POST"
        }).done(function(data) {
            switch (data.status_code) {
                case 500:
                    iziToast.error({
                        title: 'Error',
                        message: data.message,
                        position: 'topRight'
                    });
                    break;
                case 200:
                    iziToast.success({
                        title: 'Success',
                        message: data.message,
                        position: 'topRight',
                    });
                    break;
                default:
                    break;
            }
        });
    }
</script>
@endpush

