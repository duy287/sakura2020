ECHO OFF

TITLE Clear cache on Laravel
:: This scipts only clear Laravel cache on Windows OS.
:: Reoptimized class loader:
:: php artisan optimize

::Clear Cache facade value:
php artisan cache:clear

::Clear Route cache:
php artisan route:clear

::Clear View cache:
php artisan view:clear

::Clear Config cache:
php artisan config:clear

::composer install --no-dev
:: Chạy trên môi trường dev, local
::npm run dev
:: Chạy khi đưa lên host, deploy app, 2 lệnh này giống nhau
::npm run prod
::npm run production
::Chạy khi vừa dev, vừa chỉnh các file assets
:: npm run watch
