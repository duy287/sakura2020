<?php
use Carbon\Carbon;
use App\Facades\SettingFacade;
use App\Supports\SettingStore;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
if (!function_exists('format_date')) {
    /**
     * @param $time
     * @param string $format
     * @return mixed
     */
    function format_date($time, $format = 'Y-m-d')
    {
        if (empty($time)) {
            return "";
        }
        return Carbon::parse($time)->format($format);
    }
}

if (!function_exists('image_thumb')) {
    /**
     * @param $time
     * @param string $format
     * @return mixed
     */
    function image_thumb($path, $width=60)
    {
        return view('items.img_thumb', compact('path','width'));
    }
}

if (!function_exists('status_badge')) {
    /**
     * @param $time
     * @param string $format
     * @return mixed
     */
    function status_badge($item)
    {
        return view('items.status_badge', compact('item'));
    }
}

if (!function_exists('status_checkbox')) {
    /**
     * @param $time
     * @param string $format
     * @return mixed
     */
    function status_checkbox($item)
    {
        return view('items.status_checkbox', compact('item'));
    }
}
/**
 * @return boolean
 * @author Minh Hao
 */
function check_database_connection()
{
    try {
        DB::connection(config('database.default'))->reconnect();
        return true;
    } catch (Exception $ex) {
        return false;
    }
}

if (!function_exists('setting')) {
    /**
     * Get the setting instance.
     *
     * @param $key
     * @param $default
     * @return SettingStore|string|null
     * @author Minh Hao
     */
    function setting($key = null, $default = null)
    {

        if (!empty($key)) {

            try {
                return Setting::get($key, $default);

            } catch (Exception $exception) {
                info($exception->getMessage());
                return $default;
            }
        }
        return SettingFacade::getFacadeRoot();
    }
}
if (! function_exists('array_dot')) {
    /**
     * Flatten a multi-dimensional associative array with dots.
     *
     * @param  array   $array
     * @param  string  $prepend
     * @return array
     */
    function array_dot($array, $prepend = '')
    {
        return Arr::dot($array, $prepend);
    }
}
if (! function_exists('execute_command')) {
    /**
     * Flatten a multi-dimensional associative array with dots.
     *
     * @param string $command
     * @param array $parameters
     * @param null $outputBuffer
     * @return bool|int
     * @throws Exception
     */

    function execute_command(string $command, array $parameters = [], $outputBuffer = null)
    {

        if (!function_exists('proc_open')) {
            if (config('app.debug')) {
                throw new Exception('Function proc_close() is disabled. Please contact your hosting provider to enable it.');
            }
            return false;
        }

        return Artisan::call($command, $parameters, $outputBuffer);
    }
}
