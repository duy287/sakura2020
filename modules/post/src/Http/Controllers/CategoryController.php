<?php

namespace Modules\Post\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Post\Repositories\Contracts\ICategory;
use Modules\Post\Repositories\Contracts\IPost;
use Modules\Post\Http\Requests\CategoryRequest;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    protected $cateRepository, $postRepository;

    public function __construct(ICategory $cateRepository, IPost $postRepository)
    {
        $this->cateRepository = $cateRepository;
        $this->postRepository = $postRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->cateRepository->datatable();
        }
        return view('Post::category.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = $this->cateRepository->all();
        return view('Post::category.add', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $data = $request->except(['_token']);
        $data['slug'] = Str::slug($request->slug);
        $this->cateRepository->create($data);
        
        return redirect()->route('categories.index')->with([
            'flash_level'   => 'success',
            'flash_message' => trans('action.add_success', ['m'=>'category'])
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = $this->cateRepository->getWhere('id', '!=', $id);
        $category = $this->cateRepository->find($id);
        return view('Post::category.edit', compact('category', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $id)
    {
        $data = $request->except(['_token', '_method']);
        $data['slug'] = Str::slug($request->slug);
        $this->cateRepository->update($id, $data);

        return redirect()->route('categories.index')->with([
            'flash_level'   => 'success',
            'flash_message' => trans('action.update_success', ['m'=>'category'])
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($this->postRepository->where('category_id', $id)->count() > 0){
            return response()->json([
                'row_id' => $id,
                'message' => 'You cannot delete this category because it has posts',
                'status_code' => 500
            ]);
        }
        if($this->cateRepository->delete($id)){
            return response()->json([
                'row_id' => $id,
                'message' => trans('action.delete_success', ['m'=>'category']),
                'status_code' => 200
            ]);
        }
        else{
            return response()->json([
                'row_id' => $id,
                'message' => trans('action.has_error'),
                'status_code' => 500
            ]);
        }
    }

    /**
     * Update status the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateStatus($id){
        $category = $this->cateRepository->find($id);
        if($category->status==1)
            $status = 0;
        else 
            $status = 1;
        if($this->cateRepository->update($id, ['status'=> $status])){
            return response()->json([
                'status' => $status,
                'message'=> trans('action.update_success', ['m'=> 'status']),
                'status_code' => 200
            ]);
        }
        else{
            return response()->json([
                'message' => trans('action.has_error'),
                'status_code' => 500
            ]);
        }
    }
}
