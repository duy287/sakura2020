<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('lib/adminlte/css/adminlte.min.css')}}">
    <style>
        canvas {
            background-color: #e9ecef;
            display: block;
            position: fixed;
            top: 0;
            left: 0;
            z-index: -9999;

        }
        .login-logo{
            font-size: 35px;
            text-align: center;
            margin-bottom: 25px;
            font-weight: 300;
        }
        .card-header{
            color: #fff;
            font-size: 22px;
            text-align: center;
            background: #00c0ef;
            border-top: 3px solid #007bff;
        }
        body {
            z-index: -9999;
            margin-top: 80px;
            margin-bottom: auto;
            overflow: hidden;
        }
        label:not(.form-check-label):not(.custom-file-label){
            font-weight: normal;
        }
    </style>
</head>
<body>
    <!-- background animate -->
    <canvas id="dot-connect"></canvas>

    <div id="app">
        <div class="login-logo ">
            <a href="#"><b class="text-primary font-weight-bold">SAKURA</b>-ADMIN</a>
        </div>
        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <!-- dot-connect -->
    <script src="{{asset('lib/canvas-animation/dot-connect.min.js')}}"></script>
</body>
</html>
