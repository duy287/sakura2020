<?php
namespace App\Repositories\Eloquent;
use App\Models\User;
use App\Repositories\Contracts\IUser;
use Illuminate\Support\Facades\Storage;

class UserRepository extends BaseRepository implements IUser 
{
    public function model(){
        return User::class ;
    }
}
