<?php
return [
    'categories' => 'Categories',
    'category_list' => 'Category list',
    'add_category' => 'Add new category',
    'edit_category' => 'Edit category',
    'slug'  => 'Slug',
    'status' => 'Status',
    'active' => 'Active',
    'parent' => 'Parent',
    'name' => 'Name',
    'permalink' => 'Permalink'
];